package com.cappable.shades;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by dimafet001 on 23.09.15.
 */
public class StartSeviceOnReboot extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
            context.startService(new Intent(context, NotificationService.class));
    }
}
