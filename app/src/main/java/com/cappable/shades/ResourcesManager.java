package com.cappable.shades;

import android.graphics.Color;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.font.IFont;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import java.io.IOException;

/**
 * @version 1.0
 */
public class ResourcesManager {
    //---------------------------------------------
    // VARIABLES
    //---------------------------------------------

    private static final ResourcesManager INSTANCE = new ResourcesManager();
    private static int screenWidth, screenHeight;
    private static float scaleDivider; //var for scaling everything for different phones

    public Engine engine;
    public GameActivity activity;
    public Camera camera;
    public VertexBufferObjectManager vbom;

    //---------------------------------------------
    // TEXTURES & TEXTURE REGIONS
    //---------------------------------------------

    public ITextureRegion ic_settings, ic_restart, ic_shop, ic_groups,
            ic_home_game_over, ic_money, ic_cup,
            ic_vk, ic_fb, ic_g, ic_tw,
            ic_pause, ic_play,
            ic_skip, ic_down, ic_up;
    public ITiledTextureRegion ic_sound;

    public ITextureRegion splash_region;
    private BitmapTextureAtlas splashTextureAtlas;
    private BuildableBitmapTextureAtlas tutorialTextureAtlas;

    public IFont font, font_counter, font_middle, font_title, font_splash_white, font_splash_blue;

    // MUSIC
    public Sound[] sound = new Sound[9];


    //---------------------------------------------
    // CLASS LOGIC
    //---------------------------------------------

    public void loadMenuResources() {
        loadMenuGraphics();
        loadMenuAudio();
        loadMenuFonts();
    }

    private void loadMenuGraphics() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
        BuildableBitmapTextureAtlas menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);

        //isSound is inited first because it is tiled. To avoid bugs with TextureAtlas
        ic_sound = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(menuTextureAtlas, activity, "ic_sound_tiled.png", 2, 1);
        ic_settings = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_settings.png");
        ic_shop = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_shop.png");
        ic_restart = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_restart.png");
        ic_groups = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_groups.png");

        ic_pause = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_pause.png");
        ic_play = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_play.png");

        ic_home_game_over = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_home.png");
        ic_money = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_money.png");
        ic_cup = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_cup.png");

        ic_vk = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_vk.png");
        ic_fb = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_fb.png");
        ic_tw = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_tw.png");
        ic_g = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "ic_g.png");

        try {
            menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            menuTextureAtlas.load();
        } catch (final ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }
    }

    private void loadMenuAudio() {
        SoundFactory.setAssetBasePath("mfx/");

        for (int i = 0; i < sound.length; ++i) {
            final String path;

            switch (i) {
                case 0: path = "angstrom_e2.wav";break;
                case 1: path = "angstrom_d2.wav";break;
                case 2: path = "angstrom_c2.wav";break;
                case 3: path = "angstrom_g.ogg";break;
                case 4: path = "angstrom_f.ogg";break;
                case 5: path = "angstrom_b2.wav";break;
                case 6: path = "angstrom_c.ogg";break;
                case 7: path = "angstrom_e.ogg";break;
                default:
                case 8: path = "angstrom_a2.wav";break;
            }

            try {
                sound[i] = SoundFactory.createSoundFromAsset(engine.getSoundManager(), activity, path);
                //sound[i].setVolume( .05f); //great on the highest volume, but on one bar it cannot be heard
                sound[i].setVolume( .2f); //average, Neither too good,nor bad.
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadMenuFonts() {
        FontFactory.setAssetBasePath("font/");
        final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        final ITexture pauseCounterFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        final ITexture gameOverTitleFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        final ITexture gameOverFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "oswald_regular.ttf", 50 * scaleDivider, true, Color.BLACK, 1, Color.BLACK);
        font.load();
        font_counter = FontFactory.createStrokeFromAsset(activity.getFontManager(), pauseCounterFontTexture, activity.getAssets(), "oswald_regular.ttf", 150 * scaleDivider, true, Color.BLACK, 1, Color.BLACK);
        font_counter.load();
        font_title = FontFactory.createStrokeFromAsset(activity.getFontManager(), gameOverTitleFontTexture, activity.getAssets(), "oswald_regular.ttf", 100 * scaleDivider, true, Color.BLACK, 1, Color.BLACK);
        font_title.load();
        font_middle = FontFactory.createStrokeFromAsset(activity.getFontManager(), gameOverFontTexture, activity.getAssets(), "oswald_regular.ttf", 70 * scaleDivider, true, Color.BLACK, 1, Color.BLACK);
        font_middle.load();
    }

    public void loadTutorialScene() {
        FontFactory.setAssetBasePath("font/");
        final ITexture tutorialFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        tutorialTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR);
        ic_skip = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tutorialTextureAtlas, activity, "ic_skip.png");
        ic_up = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tutorialTextureAtlas, activity, "ic_up.png");
        ic_down = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tutorialTextureAtlas, activity, "ic_down.png");

        try {
            tutorialTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            tutorialTextureAtlas.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();
        }
    }

    public void unloadTutorialScene() {
        tutorialTextureAtlas.unload();
    }

    public void loadSplashScreen() {
        FontFactory.setAssetBasePath("font/");
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        final ITexture splashFontWhite = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        final ITexture splashFontBlue = new BitmapTextureAtlas(activity.getTextureManager(), 512, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        font_splash_white = FontFactory.createStrokeFromAsset(activity.getFontManager(), splashFontWhite, activity.getAssets(), "ostrich-black.ttf",
                scaleDivider * 200, true, Color.WHITE, 1, Color.WHITE);
        font_splash_white.load();

        font_splash_blue = FontFactory.createStrokeFromAsset(activity.getFontManager(), splashFontBlue, activity.getAssets(), "ostrich-black.ttf",
                scaleDivider * 200, true, Color.rgb(144, 202, 249), 1, Color.rgb(144, 202, 249));
        font_splash_blue.load();
    }

    public void unloadSplashScreen() {
        font_splash_white.unload();
        font_splash_blue.unload();
    }

    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom
     * <br><br>
     * We use this method at beginning of game loading, to prepare Resources Manager properly,
     * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom)
    {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;

        screenWidth = (int) camera.getWidth();
        screenHeight = (int) camera.getHeight();

        scaleDivider = screenHeight / 1920f;
    }

    //---------------------------------------------
    // GETTERS AND SETTERS
    //---------------------------------------------

    public static ResourcesManager getInstance()
    {
        return INSTANCE;
    }
}