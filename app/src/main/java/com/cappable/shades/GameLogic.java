package com.cappable.shades;

import android.app.Activity;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.SingleValueSpanEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.array.ArrayUtils;
import org.andengine.util.modifier.IModifier;

import java.util.Random;

/**
 * Created by hotmeaty on 07.03.2015.
 */
public class GameLogic {

    private static GameLogic instance;
    private static Activity activity;

    boolean isSound = true;

    Random random = new Random();
    protected FallingRect[] rects = new FallingRect[36];

    private Rectangle startRect;
    private Rectangle bottomColumn;

    boolean [] allowRect = new boolean[36];
    float[][] Colors;

    boolean isPlaying, turnOff, drawStatic, wasNewRect,
            neededNewRect, firstNewLaunchStart = true;
    private boolean gameOver = false;
    float wasXTouch, wasYTouch; // for slide detection
    int screenWidth, screenHeight, screenWidthQuater, screenHeightNineth;
    boolean newRect = true, justNewRect, returningAfterPause, gamePause;
    private int justNewRectInt = 0, secondLaunch = 0;
    int RECT_I_WORK_WITH, currentRectNumber = 0, allow, waitColor = 0;
    //vel for moving the rect

    boolean drawBgColumn = false, isStarting = true;

    int[] upper = {0, 0, 0, 0};
    boolean holdTouches = false, waitNewRectColor, newScore = false;
    boolean d = true, oneTimeSoundCheckAfterFall = true, t = true, oBoolean = true;

    int h;
    int wasLevel = 1, currentLevel = 1, difficulty = 0, nextColor = 0, score = 0;
    public boolean scoreChanged = false;

    int CurrentUpperLL, CurrentUpperRR;//for drawing Starting Rect

    private Sound[] sound;

    private static VertexBufferObjectManager vbom;

    private int curSound;

    public static void newInstance(Activity mActivity, int screenWidth, int screenHeight, VertexBufferObjectManager vbom) {
        instance = new GameLogic(screenWidth, screenHeight);
        activity = mActivity;
        GameLogic.vbom = vbom;
    }

    public static GameLogic getInstance() {
        return instance;
    }

    private GameLogic(int screenWidth, int screenHeight) {
        isPlaying = turnOff = gameOver = drawStatic = wasNewRect =
        justNewRect = returningAfterPause = gamePause = false;
        neededNewRect = firstNewLaunchStart = true;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        CurrentUpperLL = CurrentUpperRR = screenWidth;
        //font and assets are given here from gameAct
        screenWidthQuater = screenWidth/4;
        screenHeightNineth = screenHeight/9;
    }

    public void setSounds(Sound[] sound) {
        this.sound = sound;
    }

    public void setColors(float[][] colors){
        Colors = colors;
    }

    public void setRects(final FallingRect[] rects, final Rectangle newColor, final Rectangle bottomColumn, double px_per_inch) {
        this.rects = rects;
        FallingRect.setRect(Colors, difficulty, screenHeight, screenWidth, px_per_inch);
        for(int i = 0; i < 36; i++){
            allowRect[i] = true;
        }

        this.startRect = newColor;
        this.bottomColumn = bottomColumn;
        setAddRects();
    }

    private void setAddRects() {
        this.bottomColumn.setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        this.startRect.setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        this.startRect.setVisible(true);

    }

    public void update() {
        //drawing Ting upper rect
        if (isPlaying && !(drawStatic || gameOver))
            if (isStarting)
                if (startRect.getX() > screenWidth / 90) {
                    startRect.setPosition(startRect.getX() - screenWidth / 90, 0);
                } else {
                    startRect.setPosition(0, 0);
                    isStarting = false;
                }
            else {
                //kinda timer for drawing next color rect
                if (waitNewRectColor && waitColor < 50 && !(gamePause || returningAfterPause))
                    waitColor += 1;
                else if (waitNewRectColor && !(gamePause || returningAfterPause)) {
                    startRect.setVisible(true);
                }

                ifNewRect();//there is checkForCombo in ifNewRect()

                for (int i = 0; i < rects.length; ++i)
                    if (rects[i].getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO || i == RECT_I_WORK_WITH)
                        rects[i].update();
            }
    }

    private int levelColorRandomiser(int currentLevel){
        switch (difficulty) {
            case 0:
                if (currentLevel == 1) {
                    int r = random.nextInt(10);
                    if (r < 6)
                        return 0;
                    else /*if (r < 90)*/
                        return 1;
//                    else
//                        return 2;
                } else if (currentLevel == 2) {
                    int r = random.nextInt(10);
                    if (r < 6)
                        return 0;
                    else if (r < 9)
                        return 1;
                    else
                        return 2;
                } else if (currentLevel == 3) {
                    int r = random.nextInt(10);
                    if (r < 4)
                        return 0;
                    else if (r < 7)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel == 4) {
                    int r = random.nextInt(11);
                    if (r < 3)
                        return 0;
                    else if (r < 6)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel < 10) {
                    int r = random.nextInt(12);
                    if (r < 3)
                        return 0;
                    else if (r < 6)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel < 15) {
                    return random.nextInt(4);
                } else {
                    return random.nextInt(5);
                }

            case 1:

                //medium
                if (currentLevel == 1) {
                    int r = random.nextInt(100);
                    if (r < 55)
                        return 0;
                    else if (r < 90)
                        return 1;
                    else
                        return 2;
                } else if (currentLevel == 2) {
                    int r = random.nextInt(10);
                    if (r < 4)
                        return 0;
                    else if (r < 8)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel == 3) {
                    int r = random.nextInt(10);
                    if (r < 4)
                        return 0;
                    else if (r < 6)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel == 4) {
                    int r = random.nextInt(11);
                    if (r < 3)
                        return 0;
                    else if (r < 6)
                        return 1;
                    else if (r < 9)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel < 8){
                    return random.nextInt(4);
                } else {
                    return random.nextInt(5);
                }
                //medium end

            case 2:
                //hard
                if (currentLevel == 1) {
                    int r = random.nextInt(10);
                    if (r < 4)
                        return 0;
                    else if (r < 8)
                        return 1;
                    else
                        return 2;
                } else if (currentLevel == 2) {
                    int r = random.nextInt(9);
                    if (r < 3)
                        return 0;
                    else if (r < 6)
                        return 1;
                    else if (r < 8)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel == 3) {
                    int r = random.nextInt(7);
                    if (r < 2)
                        return 0;
                    else if (r < 4)
                        return 1;
                    else if (r < 6)
                        return 2;
                    else
                        return 3;
                } else if (currentLevel < 7){
                    return random.nextInt(4);
                } else {
                    return random.nextInt(5);
                }
        }
        return 0;
    }

    protected void setRectStatic(boolean drawStatic) {
        this.drawStatic = drawStatic;
//        startPaint.setMaskFilter(drawStatic ? blurMaskFilter : null);
        FallingRect.drawStatic = drawStatic;
//        for(int y4 = 0; y4 < 36; y4++) {
//   //         rects[y4].drawStatic = drawStatic;
//            rects[y4].mPaint.setMaskFilter(drawStatic ? blurMaskFilter : null);
//        }
    }

    protected FallingRect getRectIWorkWith(){
        return rects[RECT_I_WORK_WITH];
    }

    protected FallingRect getRectWithState(final int col, final int row){
        for(h = 0; h < 36; h++)
            if (allowRect[h] && rects[h].getState() != FallingRect.STATE.NOTHING && rects[h].getToDrawOrNotToDraw()
                    && rects[h].getRectColumn() == col && rects[h].getRow() == row) {
                allow = h;
                return rects[h];
            }
        return null;
    }

    private boolean getAllRectsToDelete(){
        if(getRectsFallingFromCombo()) {
            boolean[] fallers = getColRectFallingFromCombo();
            for (int i = 0; i < 4; ++i) {
                if (!fallers[i]) {
                    startDeleteRect(i, upper[i]);
                }
            }

            return true;
        }
        boolean t = false;
        int[] cUpper = {upper[0], upper[1], upper[2], upper[3]};
        for(int col = 0; col < 4; ++col) {
            final boolean a = startDeleteRect(col, cUpper[col]);
            if (!t) t = a;
        }
        return  t;
    }

    private boolean startDeleteRect(int col, int upperin) {
        boolean t = false;

        if (upper[col] > 1) {
            for (int row = 1; row < upperin; row++) {

                FallingRect rect_upper = getRectWithState(col + 1, row + 1);
                FallingRect rect_lower = getRectWithState(col + 1, row);

                if(rect_upper == null || rect_lower == null)
                    return  true;
                if (rect_lower.getColorN() == rect_upper.getColorN()
                        && rect_lower.getColorN() < 4) {
                    if (rect_upper.getState() != FallingRect.STATE.FALLING_CAUSE_OF_COMBO || rect_lower.getState() != FallingRect.STATE.FALLING_CAUSE_OF_COMBO) {

                        for (Sound s : sound) s.stop();

                        if (isSound) sound[rect_upper.getColorN() + 1 ].play();

                        rect_lower.setVisible(false);
                        rect_lower.mReset();

                        allowRect[allow] = false;

                        rect_upper.setOneDown();
                        rect_upper.addColor();

                        if(upper[col] > row + 1)
                            for(int u4 = row + 2; u4 <= upper[col]; u4++)
                                getRectWithState(col + 1, u4).setLowerOne();
                        upper[col] -= 1;
                        currentRectNumber--;
                        score += 4;
                        scoreChanged = true;
                    }
                    t = true;
                }
            }
        }

        return t;
    }

    private void getAllRowsToDelete(){
        if(upper[0] > 0 && upper[1] > 0 && upper[2] > 0 && upper[3] > 0) {
            final int min_upper_col = Math.min(Math.min(upper[0], upper[1]), Math.min(upper[2], upper[3]));
            final int lvl = currentLevel;

            for (int row = 1; row <= min_upper_col; ++row)
                if (getRectWithState(1, row).getColorN() == getRectWithState(2, row).getColorN() &&
                        getRectWithState(3, row).getColorN() == getRectWithState(2, row).getColorN() &&
                        getRectWithState(3, row).getColorN() == getRectWithState(4, row).getColorN()) {
                    if (!(getRectWithState(1, row).getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO ||
                            getRectWithState(2, row).getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO ||
                            getRectWithState(3, row).getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO ||
                            getRectWithState(4, row).getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO)) {
                        getRectWithState(1, row).setLevel(lvl);
                        getRectWithState(1, row).deleteRow();

                        RECT_I_WORK_WITH = allow;
                        allowRect[allow] = false;

                        for (int i = 2; i < 5; ++i) {
                            final FallingRect f = getRectWithState(i, row);
                            f.setVisible(false);
                            f.mReset();
                            allowRect[allow] = false;
                        }


                        score += 200;
                        scoreChanged = true;

                        for (Sound s : sound) s.stop();

                        if (isSound)
                            if (random.nextBoolean()) {
                                sound[3].play();
                                sound[6].play();
                                sound[7].play();
                            } else {
                                sound[0].play();
                                sound[2].play();
                                sound[8].play();
                            }

                        for (int i = 0; i < 4; ++i)
                            if (upper[i] > row)
                                for (int j = row + 1; j <= upper[i]; ++j) {
                                    getRectWithState(i + 1, j).setLevel(lvl);
                                    getRectWithState(i + 1, j).setLowerOne();
                                }

                        for (int i = 0; i < 4; ++i)
                            --upper[i];

                        currentRectNumber -= 4;
                    }
                    return;
                }
        }
        neededNewRect = true;
    }

    private boolean getRectsFallingFromCombo(){
        for (FallingRect f : rects)
            if(f.getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO)
                return true;
        return false;
    }

    /**
     * returns columns that have combos to start combos at allowed ones
     */
    private boolean[] getColRectFallingFromCombo() {
        boolean[] fallingColumns = {false, false, false, false};

        for (FallingRect f : rects)
            if(f.getState() == FallingRect.STATE.FALLING_CAUSE_OF_COMBO)
                switch (f.getRectColumn()) {
                    case 1:
                        fallingColumns[0] = true;
                        break;
                    case 2:
                        fallingColumns[1] = true;
                        break;
                    case 3:
                        fallingColumns[2] = true;
                        break;
                    case 4:
                        fallingColumns[3] = true;
                        break;
                }

        return fallingColumns;
    }

    public void getNewRectValue() {
        newRect = getRectIWorkWith().getNewRectValue();
    }

    protected void ifNewRect(){
        if (secondLaunch < 10) secondLaunch += 1;
        if (secondLaunch > 5) getNewRectValue();

        if (justNewRect){//must help with falling of several rects
            if(justNewRectInt >= 30){
                justNewRectInt = 0;
                justNewRect = false;
            }else justNewRectInt++;

            nextColor = levelColorRandomiser(currentLevel);
            startRect.setColor(Colors[nextColor][0], Colors[nextColor][1], Colors[nextColor][2]);
        }

        //here was checkForCombo
        if(newRect && secondLaunch > 3){
            if (oneTimeSoundCheckAfterFall){
                if (isSound) {
                    switch (random.nextInt(3)) {
                        case 0:
                            curSound = 0;
                            break;
                        case 1:
                            curSound = 5;
                            break;
                        case 2:
                            curSound = 8;
                            break;
                    }
                    sound[curSound].play();
                }
                for (int i = 0; i < 4; ++i)
                    upper[i] = getRectIWorkWith().getUpper(i + 1);

                oneTimeSoundCheckAfterFall = false;
            }
            //getRectsFallingFromCombo() is checked in getAllRectsTodelete
            if(!getAllRectsToDelete())
                getAllRowsToDelete();
        }
        if(newRect && neededNewRect && !justNewRect){
            if(ArrayUtils.contains(upper, 9)) gameOver = true;

            if(secondLaunch > 5) {
                score += 2;
                scoreChanged = true;
            }
            d = true;
            wasXTouch = wasYTouch = screenHeight;
            for (int i = 0; i < 36; ++i) allowRect[i] = true;
            waitNewRectColor = false;
            waitColor = 0;
            for(int i = 0; i < 36; ++i) {
                if (newRect && rects[i].getState() == FallingRect.STATE.NO_EXIST) {

                    startRect.setVisible(false);

                    rects[i].getRandomRectParams();//setstatefalling
                    rects[i].setStartColor(nextColor);
                    rects[i].setColor(Colors[nextColor][0], Colors[nextColor][1], Colors[nextColor][2]);
                    rects[i].setLevel(currentLevel);
                    rects[i].setOK(true);
                    rects[i].setVisible(true);
                    RECT_I_WORK_WITH = i;
                    for (int j = 0; j < 36; ++j)
                        if (rects[j].getState() == FallingRect.STATE.NOTHING) {
                            rects[j].setState(FallingRect.STATE.NO_EXIST);
                        }
                    newRect = false;
                    justNewRect = true;
                    waitNewRectColor = true;
                    currentRectNumber++;
                    oneTimeSoundCheckAfterFall = true;

                    bottomColumn.setColor(Colors[getRectIWorkWith().getColorN()][0],
                            Colors[getRectIWorkWith().getColorN()][1],
                            Colors[getRectIWorkWith().getColorN()][2]);
                    break;
                }
            }
            neededNewRect = false;
        }
    }

    protected void reset(float[][] Colors){
        newScore = isPlaying = turnOff = gameOver = drawStatic = wasNewRect =
                justNewRect = returningAfterPause = drawBgColumn =
                        holdTouches = waitNewRectColor = false;
        isStarting = neededNewRect =
                firstNewLaunchStart = newRect = d = oneTimeSoundCheckAfterFall = t = oBoolean = true;
        justNewRectInt = secondLaunch = nextColor = currentRectNumber = waitColor = 0;
        for(int i = 0; i < 4; ++i)
            upper[i] = 0;

        currentLevel = 1;
        score = 0;
        scoreChanged = true;

        startRect.setPosition(screenWidth, 0);
        startRect.setVisible(true);
        this.startRect.setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        bottomColumn.setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        //ToDo: set random Color to start rect

        this.Colors = Colors;

        for(int o = 0; o < 36; o++) {
            rects[o].mReset();
        }

        FallingRect.resetAll(Colors, difficulty);
    }

    public void restart(float[][] Colors){
        reset(Colors);
        isPlaying = true;
    }

    public void pause(){
        isPlaying = false;
    }

    protected void setIsSound(boolean is) {
        isSound = is;
    }

    // ------------------------------------------------
    //              GAMEOVER THINGS
    //-------------------------------------------------

    public boolean isGameOver() {
        return gameOver;
    }

    private FallingRect[] rectsGameOverUpper = new FallingRect[4];

    public void startGameOverAnimation() {
        if (!gameOver) return;

        for (int i = 0; i < 4; ++i) {
            rectsGameOverUpper[i] = getRectWithState(i+1, upper[i]);
            iterateNext(rectsGameOverUpper[i], i, upper[i]);
        }
    }

    //GameOver method
    private void iterateNext(final FallingRect r, final int i, final int j) {
        try {
            if (r == null) throw new NullPointerException();
        } catch (NullPointerException e) {
            e.printStackTrace();
            //last time happened with Rect r
            ShadesApplication.getInstance().trackException(e);
        }
            if (j < 2) {
                //everything is fallen
                if (upper[0] < 2 && upper[1] < 2 && upper[2] < 2 && upper[3] < 2) {

                    int color = 0;

                    for (int l = 0; l < 4; ++l)
                        if (rectsGameOverUpper[l] != null && rectsGameOverUpper[l].getColorN() > color)
                            color = rectsGameOverUpper[l].getColorN();

                    for (final FallingRect rect : rectsGameOverUpper)
                        if (rect != null)
                            rect.changeColor(color, new IEntityModifier.IEntityModifierListener() {
                                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                                }

                                @Override
                                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                    rect.registerEntityModifier(new MoveYModifier(.7f, rect.getY(), screenHeight));
                                }
                            });
                }
                return;
            }
            final FallingRect rLower = getRectWithState(i + 1, j - 1);

            r.changeColor(rLower.getColorN());
            r.setHeight(r.getHeight() + screenHeightNineth);
            rLower.setVisible(false);
            r.registerEntityModifier(new MoveYModifier(.7f, r.getY(), r.getY() + screenHeightNineth));
            r.registerEntityModifier(new SingleValueSpanEntityModifier(.7f, screenHeightNineth * 2, screenHeightNineth, new IEntityModifier.IEntityModifierListener() {
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                }

                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    --upper[i];
                    r.correctCoords();
                    iterateNext(r, i, j - 1);
                }
            }) {
                protected void onSetInitialValue(IEntity pItem, float pValue) {}
                @Override
                protected void onSetValue(IEntity pItem, float pPercentageDone, float pValue) {
                    ((Rectangle) pItem).setHeight(pValue);
                }
                public IEntityModifier deepCopy() throws DeepCopyNotSupportedException {return null;}
            });
    }
}
