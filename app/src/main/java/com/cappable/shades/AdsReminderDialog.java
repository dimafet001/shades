package com.cappable.shades;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by hotmeaty on 20.06.2015.
 */
public class AdsReminderDialog extends DialogFragment implements View.OnClickListener{

    final Typeface font;
    Button btnOK;
    TextView tvDialog;
    private static final String SP_LAST_TIME_ADS_LAUNCHED = "last_ads";
    SharedPreferences sp;

    public AdsReminderDialog() {super();font = null;}
//    public AdsReminderDialog(Typeface font){
//        this.font = font;
//    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ads_reminder_dialog, container);
        sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        btnOK = (Button) view.findViewById(R.id.btnOK);
        tvDialog = (TextView) view.findViewById(R.id.tvDialog);

        tvDialog.setText(new Random().nextBoolean() ? R.string.ads_remind_1 : R.string.ads_remind_2);

//        btnOK.setTypeface(font);
//        tvDialog.setTypeface(font);
        btnOK.setOnClickListener(this);

        Dialog dialog = getDialog();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        return view;
    }

    @Override
    public void onClick(View v) {
        getDialog().dismiss();
        sp.edit().putInt(SP_LAST_TIME_ADS_LAUNCHED, 0).apply();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        getDialog().dismiss();
        sp.edit().putInt(SP_LAST_TIME_ADS_LAUNCHED, 0).apply();
    }
}