package com.cappable.shades;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by dimafet on 1/13/16.
 */
public abstract class mToggleButtonSprite extends TiledSprite {

    boolean wasTouched = false;

    public mToggleButtonSprite(float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (pSceneTouchEvent.isActionDown())
            wasTouched = true;
        else if (pSceneTouchEvent.isActionUp()) {
            if (wasTouched) {
                onClicked();
            }

            wasTouched = false;
        }
        return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
    }

    public float getBottom() {
        return getY() + getHeight();
    }

    public abstract void onClicked();
}
