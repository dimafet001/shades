package com.cappable.shades;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by root on 1/16/16.
 */
public abstract class mButtonSprite extends Sprite {

    private boolean wasTouched = false;

    public mButtonSprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (pSceneTouchEvent.isActionDown())
            wasTouched = true;
        else if (pSceneTouchEvent.isActionUp()) {
            if (wasTouched) {
                onClicked();
            }

            wasTouched = false;
        }
        return true;
    }

    public abstract void onClicked();
}
