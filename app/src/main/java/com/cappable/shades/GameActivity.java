package com.cappable.shades;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.LayoutGameActivity;

/**
 * @author dimafet on 12/11/15.
 */
public class GameActivity extends LayoutGameActivity {

    private static int SCREEN_WIDTH, SCREEN_HEIGHT;
    private ResourcesManager resourcesManager;
    private Camera camera;
    private AdView adView;
    private SharedPreferences sp;

    private final String SP_LAST_TIME_ADS_LAUNCHED = "last_ads";//if this launch of app ads arent pressed, this ++; if this > 5 (reminder)
    private final String SP_NUMBER_LAUNCH = "num_launch";
    private final String neededStr = "needed_time";
    private final String newComers = "new_users";

    @Override
    protected void onCreate(Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
//        return new Engin(pEngineOptions, 60);
        return super.onCreateEngine(pEngineOptions);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
//        final EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(SCREEN_WIDTH, SCREEN_HEIGHT), camera);

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        SCREEN_WIDTH = size.x;
        SCREEN_HEIGHT = size.y;

        camera = new Camera(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

        final EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED,
                new RatioResolutionPolicy(SCREEN_WIDTH, SCREEN_HEIGHT), camera);
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {
        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
        resourcesManager = ResourcesManager.getInstance();
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
        SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback);
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        getSharedPreferences("notifShades", 0).edit().remove(neededStr).commit();
        Intent i = new Intent(this, NotificationService.class);
        i.putExtra("from_menu_with_love", true);
        startService(i);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SceneManager sm = SceneManager.getInstance();
        if(sm.getCurrentSceneType() == SceneManager.SceneType.SCENE_MENU &&
                !((MainMenuScene)sm.getCurrentScene()).isPaused && ((MainMenuScene)sm.getCurrentScene()).isGameplay
                && !((MainMenuScene)sm.getCurrentScene()).logic.isGameOver()) {
            ((MainMenuScene)sm.getCurrentScene()).onClick(MainMenuScene.Buttons.PAUSE);
        }
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
        mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                mEngine.unregisterUpdateHandler(pTimerHandler);

                // here it choses whether to load menu or tutorial

                if (sp.getInt(SP_NUMBER_LAUNCH, 0) < 5 && sp.getInt(SP_NUMBER_LAUNCH, 0) > 0) {
                    sp.edit().putInt(SP_NUMBER_LAUNCH, sp.getInt(SP_NUMBER_LAUNCH, 0) + 1).apply();
                }

                //tutorial_need check
                boolean startTutorial = false;
                if (sp.getInt(SP_NUMBER_LAUNCH, 0) == 0) startTutorial = true;

                if (!startTutorial) {

                    SceneManager.getInstance().createMenuScene();

                    //-------------------------------------AD HAPPENS---------------------------------//
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adView = (AdView) findViewById(R.id.adView);
                            //ToDo: samsung test string
                            final AdRequest adRequest = new AdRequest.Builder().build();
                            adView.loadAd(adRequest);
                            //listeners are set in cappable classes
                            adView.setAdListener(new AdListener() {
                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    if (!FallingRect.drawStatic)
                                        adView.setVisibility(View.GONE);
                                }
                            });
                            adView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PreferenceManager.getDefaultSharedPreferences(GameActivity.this).edit().putInt(SP_LAST_TIME_ADS_LAUNCHED, 0).apply();
                                }
                            });

                            // for fixing bug when before click ad isn't seen
                            adView.setBackgroundColor(Color.TRANSPARENT);
                        }
                    });
                    //--------------------------------------------------------------------------------//

                } else {
                    //This tracks the very first launch
                    if (sp.getBoolean(newComers, true)) {
                        ShadesApplication.getInstance().trackEvent("FirstLaunch", "TutorialStart", "New Comers");
                        sp.edit().putBoolean(newComers, false).commit();
                    }

                    SceneManager.getInstance().createTutorialScene(false);
                    final TutorialScene scene = (TutorialScene)SceneManager.getInstance().getCurrentScene();
                    scene.registerUpdateHandler(new IUpdateHandler() {
                        @Override
                        public void onUpdate(float pSecondsElapsed) {
                            if (scene.isEnd()) {
                                sp.edit().putInt(SP_NUMBER_LAUNCH, sp.getInt(SP_NUMBER_LAUNCH, 0) + 1).apply();

                                scene.clearUpdateHandlers();

                                SceneManager.getInstance().createMenuScene();
                                SceneManager.getInstance().disposeTutorialScene();
                            }
                        }
                        public void reset() {}
                    });
                }
            }
        }));
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.game_layout;
    }

    @Override
    protected int getRenderSurfaceViewID() {
        return R.id.SurfaceViewId;
    }
}
