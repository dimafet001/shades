package com.cappable.shades;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by hotmeaty on 20.06.2015.
 */
public class RateUsDialog extends DialogFragment implements View.OnClickListener {
    Button never, notNow, rate;
    RelativeLayout dialogRateLayout;
    TextView tvRateDialog;
    final Typeface font;
    private static final String SP_REMINDER_RATE = "rate_us";
    private static final String SP_REMINDER_RATE_NO = "never_ask_to_rate";
    SharedPreferences sp;

//    public RateUsDialog(Typeface font) {
//        this.font = font;
//    }
    public RateUsDialog() {
        super();font = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_us_dialog, container);

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        never = (Button) view.findViewById(R.id.btnNever);
        notNow = (Button) view.findViewById(R.id.btnNotNow);
        rate = (Button) view.findViewById(R.id.btnRate);
//        dialogRateLayout = (RelativeLayout) view.findViewById(R.id.dialogRateLayout);
        tvRateDialog = (TextView) view.findViewById(R.id.tvRateDialog);

//        dialogRateLayout.setBackgroundColor(getResources().getColor(R.color.dialogs));
//        tvRateDialog.setTypeface(font);
//        rate.setTypeface(font);
//        notNow.setTypeface(font);
//        never.setTypeface(font);
        rate.setOnClickListener(this);
        never.setOnClickListener(this);
        notNow.setOnClickListener(this);

        Dialog dialog = getDialog();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRate://pos
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse
                        ("market://details?id=com.cappable.shades"));
                startActivity(intent);
            case R.id.btnNever://neg
                sp.edit().putBoolean(SP_REMINDER_RATE_NO, true).apply();
                break;
            case R.id.btnNotNow://neu
                break;
        }
        getDialog().dismiss();
        sp.edit().putInt(SP_REMINDER_RATE, 0).apply();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        getDialog().dismiss();
        sp.edit().putInt(SP_REMINDER_RATE, 0).apply();
    }
}
