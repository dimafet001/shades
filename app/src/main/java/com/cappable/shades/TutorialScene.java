package com.cappable.shades;

import android.animation.Animator;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.LayoutGameActivity;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

/**
 * Created by root on 1/24/16.
 */
public class TutorialScene extends BaseScene {

    private mButtonSprite skip;
    protected TextView text;

    private final boolean fromMainScene;
    private boolean end = false;
    /* When everything ends, this var = true. Update handler catches from activity
       and changes scene to menu */

    final float[] blue1 = {0.7333333333333333f, 0.8705882352941177f, 0.984313725490196f};
    final float[] blue2 = {0.5647058823529412f, 0.792156862745098f, 0.9764705882352941f};
    final float[] blue3 = {0.2588235294117647f, 0.6470588235294118f, 0.9607843137254902f};
    final float[] blue4 = {0.11764705882352941f, 0.5333333333333333f, 0.8980392156862745f};
    final float[] blue5 = {0.08235294117647059f, 0.396078431372549f, 0.7529411764705882f};
    final float[] blueBg = {1.0f, 0.9764705882352941f, 0.7686274509803921f};
    final float[] blueBgLite = {1.0f, 0.9921568627450981f, 0.9058823529411765f};

    private final float[][] Colors = new float[][] {blue1, blue2, blue3, blue4, blue5, blueBg, blueBgLite};


    private final float screenChangeDuration = 1.5f;
    private final float fallScreenHeight = 3f;
    private float timePassed = 0f; //in seconds
//    private GameLogicTutorial tutorial = GameLogicTutorial.newInstance(vbom);
    private Rectangle[] rects = new Rectangle[6];
    private Sprite ic_up, ic_down;

    private boolean doubleBackPress = false;
    private HUD gameHud;

    enum STATE {
        BEGINNING,//the very
        QUESTION_DID_ASKED,//transition to the second scene
        TWO_COLOR_COMBO_DID,//before the third scene
        CAN_CHANGE_COLUMN,
        DID_CHANGED_COLUMN,
        CAN_SPEED_UP,
        DID_SPEED_UP,
        FINAL
    }

    private STATE currentState = STATE.BEGINNING;

    private boolean didChangeColumn = false, didSpeedUp = false;
    private float wasX, wasY;

    private boolean[] doHandler = {true, true, true, true};

    public TutorialScene(boolean fromMainScene) {
        super();

        //It's here. Not in createScene, as it didn't want to work there. Threw an Exception. DNKWHY
        setBackground(new Background(new Color(Colors[6][0], Colors[6][1], Colors[6][2])));

        this.fromMainScene = fromMainScene;
    }

    @Override
    public void createScene() {
        gameHud = new HUD();

        //Seventh tutorial is the longest.
        skip = new mButtonSprite(0, 0, resourcesManager.ic_skip, vbom) {
            @Override
            public void onClicked() {
                doSkip();
            }
        };

        // button is by its width five.five times less than screen width

        skip.setScale((SCREEN_WIDTH / 5.5f) / skip.getWidth());
        skip.setX((SCREEN_WIDTH - skip.getWidth()) / 2);
        skip.setY((float) px_per_inch / 2);
        skip.setAlpha(0f);
        skip.setVisible(false);

        gameHud.attachChild(skip);

        camera.setHUD(gameHud);

        final Typeface font = Typeface.createFromAsset(activity.getAssets(), "font/oswald_regular.ttf");

        text = (TextView) activity.findViewById(R.id.tvTutorial);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setTypeface(font);
                text.setText(fromMainScene ? R.string.first_tutorial_again : R.string.first_tutorial);
                text.setVisibility(View.VISIBLE);
                text.setAlpha(0f);
                text.invalidate();
            }
        });

        ((LayoutGameActivity)activity).runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                rects[rects.length - 1] = new Rectangle(SCREEN_WIDTH_QUARTER, 0, SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT, vbom);
                rects[rects.length - 1].setColor(Colors[5][0], Colors[5][1], Colors[5][2]);
                rects[rects.length - 2] = new Rectangle(SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH / 12f,
                        SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT_NINTH / 12f, vbom);
                rects[rects.length - 2].setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
                rects[rects.length - 3] = new Rectangle(0f, 0f, SCREEN_WIDTH, SCREEN_HEIGHT_NINTH/4f, vbom);
                rects[rects.length - 3].setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
                ((LayoutGameActivity) activity).runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        rects[rects.length - 1].setVisible(false);
                        rects[rects.length - 2].setVisible(false);
                        rects[rects.length - 3].setVisible(false);
                    }
                });
                attachChild(rects[rects.length - 1]);
                attachChild(rects[rects.length - 2]);
                attachChild(rects[rects.length - 3]);
            }
        });

        setVisText(true);
    }

    @Override
    public void onBackKeyPressed() {
        if (!doubleBackPress) {
            //not to let text change after restart from menu

            doubleBackPress = true;
            Toast.makeText(activity, R.string.tutorial_back_press, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackPress = false;
                }
            }, 1000);
        } else {
            if (!fromMainScene)
                System.exit(0);
            else {
                for (int i = 0; i < doHandler.length; ++i) doHandler[i] = false;
                end = true;
            }
        }
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_TUTORIAL;
    }

    @Override
    public void disposeScene() {
        end = false;
        setBackgroundEnabled(false);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(R.string.first_tutorial_again);
                text.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        timePassed += pSecondsElapsed;

        //in 8 sec change text

        if (timePassed > 8 && currentState.compareTo(STATE.QUESTION_DID_ASKED) < 0) {
            currentState = STATE.QUESTION_DID_ASKED;

            setVisSkip(true);


            for (int i = 0; i < 2; ++i) {
                rects[i] = new Rectangle(SCREEN_WIDTH * 7f / 4f,
                        i == 0 ? -(SCREEN_HEIGHT/12f)
                                : SCREEN_HEIGHT * 8f / 9f
                        , SCREEN_WIDTH / 4f, SCREEN_HEIGHT/9f, vbom);
                rects[i].setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
                attachChild(rects[i]);
            }

            //These two modifiers kinda make screen slide
            slideLeft(rects[1], false);
            rects[0].registerEntityModifier(new MoveXModifier(screenChangeDuration, rects[0].getX(), rects[0].getX() - SCREEN_WIDTH,
                    new IEntityModifier.IEntityModifierListener() {
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}

                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {


                            //starts fall of Rectangle which at the end makes kind of combo with
                            //color change

                            rects[0].registerEntityModifier(
                                    new MoveYModifier(fallScreenHeight, rects[0].getY(),
                                            rects[1].getY() - SCREEN_HEIGHT / 9f,
                                            new IEntityModifier.IEntityModifierListener() {
                                                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                                                @Override
                                                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {

                                                    rects[0].setHeight(SCREEN_HEIGHT * 2f / 9f);
                                                    rects[0].registerEntityModifier(new ParallelEntityModifier(
                                                            new ColorModifier(.3f, rects[0].getColor(),
                                                                    new Color(Colors[1][0], Colors[1][1], Colors[1][2])),
                                                            new MoveYModifier(1f, rects[0].getY(), SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH,
                                                                    new IEntityModifier.IEntityModifierListener() {
                                                                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                                                                        @Override
                                                                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                                                            currentState = STATE.TWO_COLOR_COMBO_DID;

                                                                            startThirdScreen();
                                                                        }
                                                                    })
                                                    ));
                                                    rects[1].setVisible(false);
                                                }
                                            }));
                        }
                    }));

            //After finish it changes text
            changeText(R.string.second_tutorial);
        }
    }

    //Made it just not to make tabful inner modifier list
    private void startThirdScreen() {
        changeText(R.string.third_tutorial);

        //Now top rect
        rects[2] = new Rectangle(SCREEN_WIDTH * 5f / 4f, -(SCREEN_HEIGHT/12f),
                SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT_NINTH, vbom);
        rects[2].setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        attachChild(rects[2]);

        //Bot rect
        rects[1].setX(rects[1].getX() + SCREEN_WIDTH);
        rects[1].setVisible(true);

        slideLeft(rects[0], true);
        slideLeft(rects[1], false);
        rects[2].registerEntityModifier(new MoveXModifier(screenChangeDuration,
                rects[2].getX(), rects[2].getX() - SCREEN_WIDTH, new IEntityModifier.IEntityModifierListener() {
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {

                //Showing background hints
                rects[rects.length - 1].setVisible(true);
                rects[rects.length - 2].setVisible(true);
                rects[rects.length - 3].setVisible(true);

                //falling half height to show bg columns
                rects[2].registerEntityModifier(new MoveYModifier(fallScreenHeight / 2f, rects[2].getY(), SCREEN_HEIGHT * 7f / 18f, new IEntityModifier.IEntityModifierListener() {
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        //show icon down and up
                        //up is static, while down is changed in touchevents
                        final float divider = (float) px_per_inch / 3f / resourcesManager.ic_up.getHeight();

                        ic_up = new Sprite(SCREEN_WIDTH_QUARTER/2f, SCREEN_HEIGHT_NINTH/4f
                                + (float)px_per_inch / 16f - resourcesManager.ic_up.getHeight() * (1f - divider)/2f,
                                resourcesManager.ic_up, vbom);
                        ic_up.setScale(divider);
                        attachChild(ic_up);

                        ic_down = new Sprite(SCREEN_WIDTH_QUARTER + (SCREEN_WIDTH_QUARTER - resourcesManager.ic_down.getWidth()) / 2f,
                                SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH/12f - resourcesManager.ic_down.getHeight() * (1 - (1f - divider)/2f) - (float)px_per_inch/16f,
                                resourcesManager.ic_down, vbom);
                        ic_down.setScale(divider);
                        attachChild(ic_down);

                        //blinkeng icons
                        ic_down.registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(
                                new AlphaModifier(.4f, 1f, 0f), new AlphaModifier(.2f, 0f, 1f)
                        ), 3, new IEntityModifier.IEntityModifierListener() {
                            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                ic_up.registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(
                                        new AlphaModifier(.4f, 1f, 0f), new AlphaModifier(.2f, 0f, 1f)
                                ), 3));
                            }
                        }));

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (doHandler[0]){
                                            currentState = STATE.CAN_CHANGE_COLUMN;
                                            changeText(R.string.forth_tutorial);
                                        }
                                    }
                                }, 6000);
                            }
                        });
                    }
                }));
            }
        }));
    }

    private void startFourthScreen() {
        slideLeft(rects[2], true);
        slideLeft(rects[rects.length - 3], true);

        changeText(R.string.fifth_tutorial);

        //bottom three rects
        rects[0].setPosition(SCREEN_WIDTH, SCREEN_HEIGHT * 8f / 9f);
        rects[0].setSize(SCREEN_WIDTH * 3f / 4f, SCREEN_HEIGHT_NINTH);
        rects[0].setColor(Colors[0][0], Colors[0][1], Colors[0][2]);
        rects[0].setVisible(true);

        rects[1].setPosition(SCREEN_WIDTH * 7f / 4f, -(SCREEN_HEIGHT / 12f));
        rects[1].setSize(SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT_NINTH);
        rects[1].setVisible(true);

        slideLeft(rects[0], false);
        rects[1].registerEntityModifier(new MoveXModifier(screenChangeDuration,
                rects[1].getX(), rects[1].getX() - SCREEN_WIDTH, new IEntityModifier.IEntityModifierListener() {
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {

                rects[1].registerEntityModifier(new MoveYModifier(fallScreenHeight / 2f, rects[1].getY(), SCREEN_HEIGHT * 7f / 18f,
                        new IEntityModifier.IEntityModifierListener() {
                            @Override
                            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                            }

                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (doHandler[1]) {
                                                    changeText(R.string.sixth_tutorial);
                                                    currentState = STATE.CAN_SPEED_UP;
                                                }
                                            }
                                        }, 3000);
                                    }
                                });
                            }
                        }));
            }
        }));

    }

    private void startFifthScreen(boolean skipped) {
        if (!skipped) slideLeft(rects[0], true);

        setVisSkip(false);

        changeText(R.string.seventh_tutorial);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (doHandler[2])
                            changeText(R.string.eighth_tutorial);
                    }
                }, 17000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (doHandler[3])
                            end = true;
                    }
                }, 20500);
            }
        });
    }

    private void doSkip() {
        //ToDo: later make skip bt sections, not all scenes
        doHandler[0] = false;
        doHandler[1] = false;

        final Rectangle rect = new Rectangle(0,0,SCREEN_WIDTH, SCREEN_HEIGHT, vbom);
        for (Rectangle r : rects) {
            if (r != null) {
                r.clearEntityModifiers();
                r.clearUpdateHandlers();
                if (r.collidesWith(rect))
                    r.registerEntityModifier(new MoveXModifier(screenChangeDuration, r.getX(), -SCREEN_WIDTH));
                else
                    r = null;
            }
        }
        if (ic_down != null) {
            if (ic_down.collidesWith(rect))
                ic_down.registerEntityModifier(new MoveXModifier(screenChangeDuration, ic_down.getX(), -SCREEN_WIDTH));
            else
                ic_down.setVisible(false);
        }
        if (ic_up != null) {
            if (ic_up.collidesWith(rect))
                ic_up.registerEntityModifier(new MoveXModifier(screenChangeDuration, ic_up.getX(), -SCREEN_WIDTH));
            else
                ic_up.setVisible(false);
        }

        startFifthScreen(true);
    }

    @Override
    public boolean onSceneTouchEvent(TouchEvent pSceneTouchEvent) {
        final float x = pSceneTouchEvent.getX();
        final float y = pSceneTouchEvent.getY();

        final int part = (int) (x / SCREEN_WIDTH_QUARTER);

        if (pSceneTouchEvent.isActionDown()) {
            wasX = x;
            wasY = y;
        }

        if (skip != null && skip.contains(x,y)) return true;


        if (currentState.compareTo(STATE.CAN_CHANGE_COLUMN) >= 0 && !didChangeColumn) {
            final float divider = (float) px_per_inch / 3f / resourcesManager.ic_up.getHeight();

            if (pSceneTouchEvent.isActionDown() || pSceneTouchEvent.isActionMove()) {
                //Draw Bg col
                rects[rects.length - 1].setX(SCREEN_WIDTH_QUARTER * part);
                rects[rects.length - 2].setX(SCREEN_WIDTH_QUARTER * part);
                if (part == 3) rects[rects.length - 2].setY(SCREEN_HEIGHT * 8f / 9f - SCREEN_HEIGHT_NINTH / 12f);
                else rects[rects.length - 2].setY(SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH / 12f);


                ic_down.setPosition(SCREEN_WIDTH_QUARTER * part + (SCREEN_WIDTH_QUARTER - resourcesManager.ic_down.getWidth()) / 2f,
                        SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH/12f - resourcesManager.ic_down.getHeight() * (1 - (1f - divider)/2f) - (float)px_per_inch/16f
                                - (part == 3 ? SCREEN_HEIGHT_NINTH : 0f));
                //Change column code
            } else if (pSceneTouchEvent.isActionUp()) {
                //Right
                if (part == 3) {
                    rects[rects.length - 1].setVisible(false);
                    rects[rects.length - 2].setVisible(false);
                    ic_down.setVisible(false);
                    detachChild(ic_down);

                    didChangeColumn = true;
                    currentState = STATE.DID_CHANGED_COLUMN;
                    rects[2].registerEntityModifier(new ParallelEntityModifier(
                            new MoveXModifier(.1f, rects[2].getX(), SCREEN_WIDTH_QUARTER * 3f),
                            new MoveYModifier(fallScreenHeight / 2f,
                                    rects[2].getY(), SCREEN_HEIGHT - 2 * SCREEN_HEIGHT_NINTH,
                                    new IEntityModifier.IEntityModifierListener() {
                                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                                        @Override
                                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                            //start combo
                                            rects[2].setHeight(SCREEN_HEIGHT * 2f / 9f);
                                            rects[2].registerEntityModifier(new ParallelEntityModifier(
                                                    new ColorModifier(.3f, rects[2].getColor(),
                                                            new Color(Colors[1][0], Colors[1][1], Colors[1][2])),
                                                    new MoveYModifier(1f, rects[2].getY(), SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH,
                                                            new IEntityModifier.IEntityModifierListener() {
                                                                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                                                                @Override
                                                                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                                                    ((LayoutGameActivity)activity).runOnUpdateThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            ic_up.setVisible(false);
                                                                            detachChild(ic_up);
                                                                        }
                                                                    });

                                                                    currentState = STATE.TWO_COLOR_COMBO_DID;

                                                                    startFourthScreen();
                                                                }
                                                            })
                                            ));
                                            rects[1].setVisible(false);
                                        }
                                    })
                    ));
                } else { //set default pos
                    rects[rects.length - 1].setX(SCREEN_WIDTH_QUARTER);
                    rects[rects.length - 2].setX(SCREEN_WIDTH_QUARTER);
                    rects[rects.length - 2].setY(SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH / 12f);
                    ic_down.setPosition(SCREEN_WIDTH_QUARTER + (SCREEN_WIDTH_QUARTER - resourcesManager.ic_down.getWidth()) / 2f,
                            SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH/12f - resourcesManager.ic_down.getHeight() * (1 - (1f - divider)/2f) - (float)px_per_inch/16f
                    );
                }
            }
        } else if (!didSpeedUp && currentState.compareTo(STATE.CAN_SPEED_UP) >= 0) {
            if (pSceneTouchEvent.isActionDown() || pSceneTouchEvent.isActionMove()) {
                rects[rects.length - 1].setX(SCREEN_WIDTH_QUARTER * part);
                rects[rects.length - 2].setX(SCREEN_WIDTH_QUARTER * part);
                if (part == 3)
                    rects[rects.length - 2].setY(SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH / 12);
                else
                    rects[rects.length - 2].setY(SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH / 12 - SCREEN_HEIGHT_NINTH);

                rects[rects.length - 1].setVisible(true);
                rects[rects.length - 2].setVisible(true);
            } else if (pSceneTouchEvent.isActionUp()) {
                rects[rects.length - 1].setVisible(false);
                rects[rects.length - 2].setVisible(false);
            }

            if (pSceneTouchEvent.isActionMove() && wasY < y - px_per_inch * 3f / 4f && part == 3) {
                currentState = STATE.DID_SPEED_UP;
                didSpeedUp = true;

                rects[rects.length - 1].setVisible(false);
                rects[rects.length - 2].setVisible(false);

                rects[1].registerEntityModifier(new MoveYModifier(fallScreenHeight / 8f, rects[1].getY(),
                        SCREEN_HEIGHT * 8f / 9f, new IEntityModifier.IEntityModifierListener() {
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    }

                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        rects[0].setWidth(SCREEN_WIDTH);
                        rects[1].setVisible(false);
                        rects[0].registerEntityModifier(new MoveYModifier(1f,
                                rects[0].getY(), SCREEN_HEIGHT, new IEntityModifier.IEntityModifierListener() {
                            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                            }

                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                currentState = STATE.FINAL;

                                startFifthScreen(false);
                            }
                        }));
                    }
                }));
            }
        } else if (timePassed < 8)
            timePassed = 8.0001f;


        return super.onSceneTouchEvent(pSceneTouchEvent);
    }

    public boolean isEnd() {
        return end;
    }

    private void setVisText(final boolean vis) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.animate().alpha(vis ? 1f : 0f).setDuration(400);
            }
        });
    }

    private void setVisSkip(final boolean vis) {
        if (vis) {
            skip.registerEntityModifier(new AlphaModifier(.7f, 0f, .8f));
            skip.setVisible(true);
            gameHud.registerTouchArea(skip);
        } else
            skip.registerEntityModifier(new AlphaModifier(.4f, 1f, 0f, new IEntityModifier.IEntityModifierListener() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    gameHud.unregisterTouchArea(skip);
                }

                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    skip.setVisible(false);
                }
            }));
    }

    private void changeText(final int resId) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.animate().alpha(0f).setDuration(400).setListener(new Animator.AnimatorListener() {
                    public void onAnimationStart(Animator animation) {}
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        text.setText(resId);
                        text.animate().alpha(1f).setDuration(400);
                    }
                    public void onAnimationCancel(Animator animation) {}
                    public void onAnimationRepeat(Animator animation) {}
                });
            }
        });
    }

    private void slideLeft(final Rectangle r, final boolean isGoingAway) {
        r.registerEntityModifier(new MoveXModifier(screenChangeDuration, r.getX(), r.getX() - SCREEN_WIDTH, new IEntityModifier.IEntityModifierListener() {
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                if (isGoingAway) r.setVisible(false);
            }
        }));
    }
}
