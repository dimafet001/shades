package com.cappable.shades;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by dimafet001 on 23.09.15.
 */
public class NotificationService extends Service {

    private NotificationManager nm;
    private Handler h = new Handler();
    private long abs_notif_time = 0;
    SharedPreferences sp;
    private final String neededStr = "needed_time";

    @Override
    public void onCreate() {
//        Log.d("service", "create");
        super.onCreate();
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        sp = getSharedPreferences("notifShades", 0);
        //if service was interrupted
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //turning off previous notification
        final boolean fromMenu;
        if (intent != null && intent.getExtras() != null)
            fromMenu = intent.getBooleanExtra("from_menu_with_love", false);
        else fromMenu = false;

        h.removeCallbacks(run);
        abs_notif_time = sp.getLong(neededStr, 0);


        h.postDelayed(run, (fromMenu || abs_notif_time == 0 || abs_notif_time - Calendar.getInstance().getTime().getTime() <= 0 ?
                getFutureTime() : abs_notif_time) - Calendar.getInstance().getTime().getTime());

        return super.onStartCommand(intent, flags, startId);
    }

    final Runnable run = new Runnable() {
        @Override
        public void run() {
            sendNotif();

            //for next time setting new time intead of old-cur
            abs_notif_time = 0;
            sp.edit().remove(neededStr).apply();
        }
    };

    private long getFutureTime() {
        Calendar c = Calendar.getInstance();//once in 2 days if not entering app
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)//it will be changed by str below
                , new Random().nextInt(8) + 11, new Random().nextInt(60), new Random().nextInt(60));
        c.add(Calendar.DAY_OF_MONTH, 2);
//        c.add(Calendar.SECOND, 15);

        abs_notif_time = c.getTime().getTime();
        sp.edit().putLong(neededStr, abs_notif_time).apply();
        return abs_notif_time;
    }

    void sendNotif() {
        //This is the same code for both deprecated and new ver.
        Intent intent = new Intent(this ,GameActivity.class);

        //This needed in case if activity is
        // already launched it does not launch it again
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //------------------------
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 141,
                intent, 0);
        CharSequence navBar = getText(R.string.we_miss_you);
        CharSequence title = getText(R.string.train_self);
        CharSequence text = getText(R.string.time_to_beat_score);
        //----------------------------------------------------

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Notification.Builder nb = new Notification.Builder(this);

            nb.setSmallIcon(R.drawable.ic_launcher)
                    .setTicker(navBar)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            nm.notify(141, nb.build());
        } else {//ToDo: test on android 4.0
            Notification notify = new Notification(R.drawable.ic_launcher, navBar
                    , System.currentTimeMillis());

            notify.flags |= Notification.FLAG_AUTO_CANCEL;
            nm.notify(141,notify);
        }

        startService(new Intent(getApplicationContext(), NotificationService.class));
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
//        Log.d("service", "destroy");
        super.onDestroy();
    }
}
