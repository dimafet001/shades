package com.cappable.shades;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * Created by root on 12/11/15.
 */
public abstract class BaseScene extends Scene {

    protected Engine engine;
    protected Activity activity;
    protected ResourcesManager resourcesManager;
    protected VertexBufferObjectManager vbom;
    protected Camera camera;
    protected final int SCREEN_WIDTH, SCREEN_HEIGHT;
    protected final int SCREEN_WIDTH_QUARTER, SCREEN_HEIGHT_NINTH;
    protected final double px_per_inch, screenInches;
    static SharedPreferences sp;
    static Resources resources;

    //--------------------------------//
    //  SHARED PREFERENCES VARIABLES  //
    //--------------------------------//

    final int fb_id = 0, tw_id = 1, go_id = 2, vk_id = 3;
    final String SP_NUMBER_LAUNCH = "num_launch";
    final String SP_LAST_DIFFICULTY = "last_difficulty";
    final String SP_SOUND = "sound";
    final String SP_LAST_TIME_ADS_LAUNCHED = "last_ads";//if this launch of app ads arent pressed, this ++; if this > 5 (reminder)
    final String SP_REMINDER_RATE = "rate_us";
    final String SP_REMINDER_RATE_NO = "never_ask_to_rate";
    final String[] SP_HIGH_SCORE = {"high_score_easy", "high_score_medium", "high_score_hard"};
    final String SP_MONEY = "money";
    final String[] SP_SUBSCRIBED = {"subscribed_FB", "subscribed_TW", "subscribed_Go", "subscribed_VK"};
    final String[] SP_LAST_SCORE = {"last_score_easy", "last_score_medium", "last_score_hard"};
    final String SP_CURRENT_COLOR_ARRAY = "c_array";
    final String SP_DID_TEACH_TO_SUBSCRIBE = "teached_to_subscribe"; //true when was tutorial of subscribing



    public BaseScene() {
        this.resourcesManager = ResourcesManager.getInstance();
        this.engine = resourcesManager.engine;
        this.activity = resourcesManager.activity;
        this.vbom = resourcesManager.vbom;
        this.camera = resourcesManager.camera;

        Point p = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(p);
        SCREEN_WIDTH = p.x;
        SCREEN_HEIGHT = p.y;

        SCREEN_WIDTH_QUARTER = SCREEN_WIDTH/4;
        SCREEN_HEIGHT_NINTH = SCREEN_HEIGHT/9;

        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(SCREEN_WIDTH/dm.xdpi, 2);
        double y = Math.pow(SCREEN_HEIGHT / dm.ydpi, 2);
        screenInches = Math.sqrt(x + y);

        px_per_inch = Math.sqrt(Math.pow(SCREEN_WIDTH, 2) + Math.pow(SCREEN_HEIGHT, 2)) / screenInches;

        Log.v("SCREEN", "px_per_inch: " + px_per_inch + "\ninches: " + screenInches);

        sp = PreferenceManager.getDefaultSharedPreferences(activity);
        resources = activity.getResources();

        createScene();
    }

    public abstract void createScene();
    public abstract void onBackKeyPressed();
    public abstract SceneManager.SceneType getSceneType();
    public abstract void disposeScene();

    public final double getPx_per_inch() {
        return px_per_inch;
    }

    public final double getScreenInches() {
        return screenInches;
    }
}
