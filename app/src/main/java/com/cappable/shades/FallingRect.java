package com.cappable.shades;


import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by hotmeaty on 07.03.2015.
 */
public class FallingRect extends Rectangle{

    enum STATE {
        NO_EXIST,
        FALLING,
        STAY,
        NOTHING,
        FALLING_CAUSE_OF_COMBO,
        NEARLY_FELL
    }

    float RectL, RectR; //sides of random rect
    float futureRectL, futureRectR;
    int wasColumn;//for changing column smoothly
    boolean changeColumn = false;
    static int screenWidth, screenHeight;
    static float screenWidthQuater, screenHeightNineth;
    private static double px_per_inch;
    int currentColumn, randomColumn;
    static float VelocityStart;//= 15 in SGS4
    static float VelosityStartDivider;

    float vel;// = VelocityStart

    STATE State = STATE.NO_EXIST;
    boolean speeded = false;
    boolean isOK = false;
    static boolean drawStatic = true;

    static boolean newRect = false, neededNewRect = false;
    boolean isOKtoChangeColumn = false, wasOKtoChangeColumn = false,
    drawUpperRect = true, drawRect = false, drawBgColumn = false,
    setDown = false, setLower = false, deleteRow = false;
    static boolean returningAfterPause = false;

    static int difficulty;
    static int[] upper = {0, 0, 0, 0};
    float bottomStopPoint;
    //currentRow not needed

    protected boolean rectIsFallen = false;

    int nColor;
    static float[][] Colors;

    private static final int frames_for_passing_height_easy = 180, frames_for_passing_height_mid = 128;
    static float smallRectHeight;


    public FallingRect(final int screenWidth, final int screenHeight, final VertexBufferObjectManager pVertexBufferObjectManager) {
        super(0, 0, screenWidth/4, screenHeight/9, pVertexBufferObjectManager);
        setVisible(false);
        vel = VelocityStart;
    }

    public static void resetAll(float[][] Colors, int difficulty) {
        newRect = neededNewRect = false;
        upper = new int[]{0, 0, 0, 0};
        FallingRect.Colors = Colors;
        FallingRect.difficulty = difficulty;
        VelosityStartDivider = difficulty == 0 ? frames_for_passing_height_easy : frames_for_passing_height_mid;
        VelocityStart = difficulty == 0 ? screenHeight / VelosityStartDivider : screenHeight / VelosityStartDivider;
    }

    static void setRect(final float[][] Colors, final int mDifficulty, final int screenHeight, final int screenWidth, double px_per_inch) {
        FallingRect.screenHeight = screenHeight;
        FallingRect.screenWidth = screenWidth;
        screenWidthQuater = screenWidth / 4;
        screenHeightNineth = screenHeight / 9;
        smallRectHeight = screenHeightNineth / 12;
        FallingRect.px_per_inch = px_per_inch;

        difficulty = mDifficulty;
        VelosityStartDivider = difficulty == 0 ? frames_for_passing_height_easy : frames_for_passing_height_mid;
        VelocityStart = difficulty == 0 ? screenHeight / VelosityStartDivider : screenHeight / VelosityStartDivider;

        Arrays.fill(upper, 0);
        FallingRect.Colors = Colors;
    }

    public void setColorArr(float[][] Colors) {
        FallingRect.Colors = Colors;
    }

    public void update() {
        if (isOK)
            //draw pause state
            if (drawStatic) returningAfterPause = true;
            else {
                //code to draw upper rect, moving
                if (drawUpperRect) drawUpperRect();

                //---------------- draw Rect Itself ------------//
                if (drawRect && State == STATE.FALLING && getBottom() + vel < bottomStopPoint) {
                    isOKtoChangeColumn = wasOKtoChangeColumn =
                    drawBgColumn = true;
                    setY(getY() + vel);
                } else if (drawRect && State == STATE.FALLING && getBottom() + vel >= bottomStopPoint) {
                    isOKtoChangeColumn = false;
                    setY(bottomStopPoint - screenHeightNineth);
                    setUpper();
                    rectIsFallen = true;
                    State = STATE.NEARLY_FELL;
                } else if (rectIsFallen && !changeColumn) {
                    isOKtoChangeColumn = drawBgColumn =
                    rectIsFallen = false;
                    newRect = true;
                    State = STATE.STAY;

                    //final correct of params
                    correctCoords();
                }
                //---------------- End draw Rect Itself ------------//

                //------------- Change Column Lines ------------------//
                if (changeColumn)
                    //to right
                    if (wasColumn <= currentColumn && getX() < futureRectL - screenWidth / 15)
                        setX(getX() + screenWidth / 15);
                    //to left
                    else if (getX() > futureRectL + screenWidth / 15)
                        setX(getX() - screenWidth / 15);
                    else {
                        setX(futureRectL);
                        changeColumn = false;
                    }
                //------------- End Change ------------------//

                //------------ if's for different combos -------------//

                //combo 2 blocks
                if (setDown && getHeight() > vel + screenHeightNineth) {
                    setY(getY() + vel);
                    setHeight(getHeight() - vel);
                    newRect = false;
                } else if (setDown) {
                    State = STATE.STAY;
                    setY(getBottom() - screenHeightNineth);
                    setDown = false;
                    correctCoords();
                    newRect = neededNewRect = true;
                }

                //falling after lower blocks disappearing
                if (setLower && getBottom() + vel <= bottomStopPoint) {
                    setY(getY() + vel);
                } else if (setLower) {
                    setY(bottomStopPoint - screenHeightNineth);
                    setUpper();
                    newRect = true;
                    setLower = false;
                    State = STATE.STAY;

                    correctCoords();
                }

                if (deleteRow && vel < getHeight()) {
                    setY(getY() + vel);
                    setHeight(getHeight() - vel);
                } else if (deleteRow) {
                    setY(getBottom());
                    setHeight(0);
                    deleteRow = false;
                    State = STATE.STAY;
                    mReset();
                    for (int p = 0; p < 4; ++p)
                        upper[p] -= 1;
                }
            }
    }

    public void correctCoords() {
        setY(screenHeight - getRow() * screenHeightNineth);
        setHeight(screenHeightNineth);
    }

    private float getBottom() {
        return getY() + getHeight();
    }

    protected boolean getToDrawOrNotToDraw(){
        return isOK;
    }

    protected void setColumn(final int newCol) {
        if (isOKtoChangeColumn && !(drawStatic || drawUpperRect || changeColumn)) {
            if (checkColChangeAvailable(newCol)){
                futureRectL = screenWidthQuater * (currentColumn - 1);
                futureRectR = futureRectL + screenWidthQuater;
                changeColumn = true;/*sets true to forbid changing columns before change is done
                                  also allows to draw changing in .draw(); */
                getBottomStopPoint();
            }
        }
    }

    private boolean checkColChangeAvailable(final int newCol) {
        //1 to 4
        final int cRow = getRow();

        if (currentColumn == newCol) return false;

        //if going to the right
        if (currentColumn < newCol) for (int i = currentColumn + 1; i <= newCol; ++i) {
            if (cRow <= upper[i - 1]) {
                wasColumn = currentColumn;
                currentColumn = i - 1;
                return true;
            } else if (i == newCol && cRow > upper[newCol - 1]) {
                wasColumn = currentColumn;
                currentColumn = newCol;
                return true;
            }
        }
        //if going to the left
        else for (int i = currentColumn - 1; i >= newCol; --i) {
            if (cRow <= upper[i - 1]) {
                wasColumn = currentColumn;
                currentColumn = i + 1;
                return true;
            } else if (i == newCol && cRow > upper[newCol - 1]) {
                wasColumn = currentColumn;
                currentColumn = newCol;
                return true;
            }
        }

        return false;
    }

    protected int getUpper(int column){//to get uppers from board class
        return upper[column - 1];
    }

    protected void setUpper(){//method for setting upper at the end of move
        upper[currentColumn - 1] += 1;
    }

    public static void setDifficulty(int mDifficulty){
        difficulty = mDifficulty;
    }

    protected void setLevel(int currentLvl) {
        int divider = difficulty == 0 ? frames_for_passing_height_easy : frames_for_passing_height_mid;

        //! (dif != 0 && cl > 7)
        VelosityStartDivider = difficulty == 0 || currentLvl <= 7 ?
                divider - ((currentLvl - 1) * 5) :
                divider - ((6 * 5) + (currentLvl - 7) * 2);

        VelocityStart = vel =
                screenHeight / (VelosityStartDivider > 100 ? VelosityStartDivider : 100);
    }

    protected int getRectColumn() {
        return currentColumn;
    }

    protected int getRow() {
        for (int i = 9; i > 0; --i)
            if (getBottom() <= screenHeight - screenHeightNineth * (i - 1) + (float)px_per_inch/63f ) return i;
        return 0;
    }

    protected int getColorN() {
        return nColor;
    }

    protected void addColor() {
        if (nColor < 4) {
//            registerEntityModifier(new ColorModifier((difficulty == 2 ? 7f : 15f) * VelocityStart / screenHeightNineth,
            registerEntityModifier(new ColorModifier(.35f,
                    Colors[nColor][0], Colors[nColor + 1][0], Colors[nColor][1], Colors[nColor + 1][1],
                    Colors[nColor][2], Colors[nColor + 1][2]));
            ++nColor;
        }
    }

    protected void changeColor(int i) {
        registerEntityModifier(new ColorModifier(.35f,
                Colors[nColor][0], Colors[i][0], Colors[nColor][1], Colors[i][1],
                Colors[nColor][2], Colors[i][2]));
        nColor = i;
    }

    protected void changeColor(int i, IEntityModifier.IEntityModifierListener ie) {
        registerEntityModifier(new ColorModifier(.35f,
                Colors[nColor][0], Colors[i][0], Colors[nColor][1], Colors[i][1],
                Colors[nColor][2], Colors[i][2], ie));
        nColor = i;
    }

    protected void setStartColor(int color) {
        nColor = color;
    }

    protected void setFallingFaster() {
        if(drawRect && !(speeded || setDown || setLower || deleteRow)) {
            vel *= 4;
            speeded = true;
        }
    }

    public boolean getNewRectValue(){
        return newRect;
    }

    public void setUpperCoords(final int a, final int b, final int c, final int d){//method for setting upper at the begin from Surface class
        upper[0] = a;
        upper[1] = b;
        upper[2] = c;
        upper[3] = d;
    }

    public void setState(STATE currentState){
        this.State = currentState;
    }

    public STATE getState(){
        return this.State;
    }

    //this method is used for tutorial
    void setRandomRectParams(int neededColumn/*from 0 to 3*/) {
//        RectT = (-screenHeightNineth)*3/4;
//        RectB = screenHeightNineth/4;
        RectL = screenWidthQuater * neededColumn;
        RectR = RectL + screenWidthQuater;
    }

    void getRandomRectParams() {
        Random random = new Random();
        randomColumn = random.nextInt(4);
        setX(0);
        setWidth(screenWidth);
        setY((-screenHeightNineth) * 3 / 4);
        setHeight(screenHeightNineth);
        currentColumn = randomColumn + 1;
        getBottomStopPoint();
        this.State = STATE.FALLING;
        newRect = false;
        RectL = randomColumn * screenWidthQuater;
        RectR = RectL + screenWidthQuater;
    }

    public void setOK(final boolean isOK) {
        //ToDo: later change to allowing to move boolean
        this.isOK = isOK;
    }

    public void setBottomStopPoint(final float bottomStopPoint) {
        this.bottomStopPoint += bottomStopPoint;
    }

    public void getBottomStopPoint() {
        this.bottomStopPoint = screenHeight - (upper[currentColumn - 1] * screenHeightNineth);
    }

    //Works on RESTART
    public void mReset() {
        State = STATE.NO_EXIST;

        drawUpperRect = true;

        drawRect = isOK = speeded =
        setDown = setLower =
        deleteRow = rectIsFallen = false;

        vel = VelocityStart;
        setVisible(false);
    }

    protected void drawUpperRect() {
        if (isOK && (RectL > getX() || RectR < getX() + getWidth())) {
            setX(getX() + RectL / 30);
            setWidth(getWidth() - (RectL + screenWidth - RectR)/30);
        } else {
            drawUpperRect = false;
            drawRect = true;
            setX(RectL);
            setWidth(screenWidthQuater);
        }
    }

    public void setOneDown() {
        setVelForCombo();

        upper[currentColumn - 1] -= 1;
        setBottomStopPoint(screenHeightNineth);
        State = STATE.FALLING_CAUSE_OF_COMBO;
        newRect = false;
        setDown = true;
        setHeight(getHeight() + screenHeightNineth);
    }

    public void setLowerOne(){
        setVelForCombo();

        upper[currentColumn - 1] -= 1;
        State = STATE.FALLING_CAUSE_OF_COMBO;
        setBottomStopPoint(screenHeightNineth);
        newRect = false;
        setLower = true;
    }

    public void deleteRow(){
        setVelForCombo();

        setX(0);
        setWidth(screenWidth);
        deleteRow = true;
        State = STATE.FALLING_CAUSE_OF_COMBO;
    }

    private void setVelForCombo(){
        vel = VelocityStart * 7 / 16;
    }
}
