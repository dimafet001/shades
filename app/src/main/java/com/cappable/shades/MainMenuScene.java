package com.cappable.shades;

import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.plus.PlusShare;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.modifier.SingleValueSpanEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by dimafet on 12/13/15.
 */
public class MainMenuScene extends BaseScene {


    enum Buttons {
        SETTINGS,
        SOUND,
        SHOP,
        RESTART,
        GROUPS,
        GAMEOVER,
        FB_SHARE,
        TW_SHARE,
        G_SHARE,
        VK_SHARE,
        PAUSE,
        TUTORIAL,
        REPORT,
        VK_GROUP,
        G_GROUP,
        TW_GROUP,
        FB_GROUP
    }

    private Text levelText, scoreText, moneyText, helpText, afterPauseCounter;
    private int afterPauseSeconds = 3;
    private TimerHandler afterPauseTimer;
    private Entity[] gameOverScreen, ic_settings;
    private Sprite[] icMenu;    //5 is number of icons
    private Sprite menuMoney, menuPlay;
    private Rectangle menuIconsRect, column, startRect, bottomColumn, rectPlayBtn, buttonRect;
    private mButtonSprite btnPause;
    private mButtonSprite[] icGroups;

    private FrameLayout frameShop;
    private ShopFragment shopFragment;
    private AdView adView;
    private Rectangle paranga;
    private HUD gameHud;

    final Animation shopToRight, shopToLeft;


    private final int settings = 0, sound = 1, shop = 2, restart = 3, groups = 4;

    private final float anim_menu_out_time = 0.25f;
    private float menu_left_x, menu_top_y, btn_pause_left, left_menu_money_ic,
            settings_margin_left, settings_x_buttons, settings_text_tutorial, settings_text_report,
            btn_play_x,
            origin_groups_x;
    private float[] origin_groups_y; // 0 is bottom icon, 4 - top

    GameLogic logic;
    private boolean isSound;


//    private int score;
    private float[][] Colors;
    private int currentColorArray = -1;
    private int difficulty;

    private float smallRectHeight;
    private boolean slideTouchLocker = true;
    private float wasXTouch, wasYTouch;


    private boolean doubleBackPress = false;
    private boolean doubleRestartPress = false;
    boolean isPaused = true, isGameplay = false, isDifficultyChanging = false;
    private boolean wasGamePlay = false, isTimerPause = false; //isgp toggles when start game and gameover.
    //for restart check
    private int wasSharedInThisGame = 0, extraMoney = 0, gotMoney;//after gameOver vars

    private boolean isOKtoClickScreen = true; //for not turning back to game after btnPause click touch
    private boolean wasGameOverDataUpdated = false, isGameOverScreenOut = false,
            isShopOut = false, isSettingsOut = false, isPlayBtnOut = false, isGroupsOut = false,
            isBeforeFirstGame = true; //for not updating the color and logic two times per first game

    private final String shareString = "Share";
    private final String openGroupString = "openGroup";
    private final String shareResultString = "shareResult";
    private final String fbString = "facebook";
    private final String twString = "twitter";
    private final String vkString = "vk";
    private final String googleString = "google+";

    public MainMenuScene() {
        super();

        boolean startTutorialSubscribe = false;

        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(SP_LAST_TIME_ADS_LAUNCHED, sp.getInt(SP_LAST_TIME_ADS_LAUNCHED, 0) + 1);
        if (!sp.getBoolean(SP_REMINDER_RATE_NO, false))
            editor.putInt(SP_REMINDER_RATE, sp.getInt(SP_REMINDER_RATE, 0) + 1);
        editor.commit();

        if (sp.getInt(SP_LAST_TIME_ADS_LAUNCHED, 0) > 7) {
            FragmentManager fragmentManager = activity.getFragmentManager();
            AdsReminderDialog adsReminderDialog = new AdsReminderDialog();
            adsReminderDialog.show(fragmentManager, "fm");
        } else if(sp.getInt(SP_REMINDER_RATE, 0) > 5 && !sp.getBoolean(SP_REMINDER_RATE_NO, false)) {
            FragmentManager fragmentManager = activity.getFragmentManager();
            RateUsDialog rateUsDialog = new RateUsDialog();
            rateUsDialog.show(fragmentManager, "fm");
        } else if(!sp.getBoolean(SP_DID_TEACH_TO_SUBSCRIBE, false)) {
            startTutorialSubscribe = true;

            sp.edit().putBoolean(SP_DID_TEACH_TO_SUBSCRIBE, true).apply();
        }

        if (startTutorialSubscribe) startTutorialSubscribe();
        else paranga.setVisible(false);

        if (!sp.contains(SP_CURRENT_COLOR_ARRAY))
            sp.edit().putInt(SP_CURRENT_COLOR_ARRAY, 0).commit();

        shopToRight = AnimationUtils.loadAnimation(activity, R.anim.shop_to_right);
        shopToLeft = AnimationUtils.loadAnimation(activity, R.anim.shop_to_left);

        shopToLeft.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        frameShop.setVisibility(View.GONE);
                    }
                });
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    // ABSTRACT EXTENDED METHODS

    @Override
    public void createScene() {
        adView = (AdView) activity.findViewById(R.id.adView);

        createHUD();
        createLogic();
        createSettings();
        setBackground(new Background(new Color(Colors[6][0], Colors[6][1], Colors[6][2])));
        createGameOver();

        frameShop = (FrameLayout) activity.findViewById(R.id.frame_shop);
        shopFragment = (ShopFragment) activity.getFragmentManager().findFragmentById(R.id.shop_fragment);
        shopFragment.setTVMoney(moneyText);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                frameShop.setLayoutParams(new RelativeLayout.LayoutParams((int) menu_left_x,
                        (int) (SCREEN_HEIGHT - adView.getHeight() - menu_top_y)));
                frameShop.setY(menu_top_y);
            }
        });
    }

    private void createHUD() {
        gameHud = new HUD();

        icMenu = new Sprite[5]; //2 is number of icons

        //---------------------- Screen Size (half in parent) ------------------------//

        menu_top_y = SCREEN_HEIGHT_NINTH < px_per_inch / 2 ?
                (float) px_per_inch / 2f :
                SCREEN_HEIGHT_NINTH * 2f;
        //size is both width and height as icons are square
        final float ic_size = (float)(screenInches * px_per_inch / 10.5f);
        final float ic_x = SCREEN_WIDTH - ic_size;

        menu_left_x = ic_x;

        Log.d("screenParams", "screenInches: " + screenInches +
                "\npx_per_inch: " + px_per_inch +
                "\nic_size: " + ic_size);
        //-----------------------------------------------------------//

        //Menu background
        final int margin = (int) (px_per_inch / 25f);
        menuIconsRect = new Rectangle(ic_x, menu_top_y - margin, ic_size, icMenu.length * (ic_size + 5) + margin * 1.5f, vbom);

        menuIconsRect.setColor(0.9450980392156862f, 0.9725490196078431f, 0.9137254901960784f, 0.4f);
        attachChild(menuIconsRect);

        //------------------- Menu Icons ----------------------//

        for (int i = 0; i < 5; ++i) {
            final ITextureRegion icon;
            final Buttons button;
            switch (i) {
                case shop:
                    icon = resourcesManager.ic_shop;
                    button = Buttons.SHOP;
                    break;
                case restart:
                    icon = resourcesManager.ic_restart;
                    button = Buttons.RESTART;
                    break;
                case groups:
                    icon = resourcesManager.ic_groups;
                    button = Buttons.GROUPS;
                    break;
                case sound:
                    //icon is inited just to compile
                    icon = resourcesManager.ic_cup;
                    button = Buttons.SOUND;
                    break;
                case settings:default:
                    icon = resourcesManager.ic_settings;
                    button = Buttons.SETTINGS;
            }

            // Sound button is toggle. That's why it is separated
            icMenu[i] = (i != sound) ? new mButtonSprite(ic_x, menu_top_y + i * (ic_size + 5f), icon, vbom) {
                @Override
                public void onClicked() {
                    if (!isTimerPause) onClick(button);
                }
            } : new mToggleButtonSprite(ic_x, menu_top_y + (ic_size + 5f), resourcesManager.ic_sound, vbom) {
                @Override
                public void onClicked() {
                    if (!isTimerPause) onClick(button);
                }
            };
        }

        isSound = sp.getBoolean(SP_SOUND, true);
        ((mToggleButtonSprite) icMenu[sound]).setCurrentTileIndex(isSound ? 1 : 0);
        //Logic.setSound is in createLogic()

        for (Sprite ic : icMenu) {
            ic.setSize(ic_size, ic_size);
            gameHud.attachChild(ic);
            gameHud.registerTouchArea(ic);
        }
        //------------------------------------------------------//

        //-------------- Texts -------------//
        final float statTextsHudY = SCREEN_HEIGHT_NINTH / 4f + 3f;

        levelText = new Text(10f, statTextsHudY, resourcesManager.font,
                resources.getString(R.string.level) + " 0123456789", vbom);
        levelText.setText(resources.getString(R.string.level) + " 1");
        gameHud.attachChild(levelText);

        scoreText = new Text(SCREEN_WIDTH/2f, statTextsHudY, resourcesManager.font, "0123456789", new TextOptions(HorizontalAlign.CENTER), vbom);
        scoreText.setText("0");
        updateTextX(scoreText);
        gameHud.attachChild(scoreText);


        menuMoney = new Sprite(0f, 0f,
                resourcesManager.ic_money, vbom);
        gameHud.attachChild(menuMoney);

        moneyText = new Text(0f, statTextsHudY, resourcesManager.font, "0123456789", new TextOptions(HorizontalAlign.CENTER), vbom);
        moneyText.setText("" + sp.getInt(SP_MONEY, 0));
        gameHud.attachChild(moneyText);

        {
            final float divider = moneyText.getHeight() / menuMoney.getHeight();
            left_menu_money_ic = SCREEN_WIDTH - resourcesManager.ic_money.getWidth() * divider - 5;
            menuMoney.setSize(menuMoney.getWidth() * divider, menuMoney.getHeight() * divider);
            menuMoney.setPosition(left_menu_money_ic, scoreText.getY() + (scoreText.getHeight() - resourcesManager.ic_money.getHeight() * divider) / 2f);
            moneyText.setX(getMoneyTextX());
        }

        helpText = new Text(SCREEN_WIDTH/2, SCREEN_HEIGHT * 3f / 4f, resourcesManager.font,
                resources.getText(R.string.press_anywhere_to_start).toString(),
                new TextOptions(HorizontalAlign.CENTER), vbom);
        updateTextX(helpText);
        helpText.setAlpha(0.3f);
        gameHud.attachChild(helpText);


        btnPause = new mButtonSprite(0, 0, resourcesManager.ic_pause, vbom) {
            @Override
            public void onClicked() {
                isOKtoClickScreen = false;
                onClick(Buttons.PAUSE);
            }
        };
        final float scalePause = (float) px_per_inch / 5.5f / btnPause.getWidth();

        btnPause.setSize(btnPause.getWidth() * scalePause, btnPause.getHeight() * scalePause);
        btnPause.setX(SCREEN_WIDTH);
        btnPause.setVisible(false);
        gameHud.attachChild(btnPause);
        gameHud.registerTouchArea(btnPause);


        afterPauseCounter = new Text(SCREEN_WIDTH/2, SCREEN_HEIGHT_NINTH*2, resourcesManager.font_counter,
                "123", new TextOptions(HorizontalAlign.CENTER), vbom);
        afterPauseCounter.setY((SCREEN_HEIGHT - helpText.getHeight()) / 2);
        updateTextX(afterPauseCounter);
        gameHud.attachChild(afterPauseCounter);
        afterPauseCounter.setVisible(false);


        menuPlay = new Sprite(0,0, resourcesManager.ic_play, vbom);

        rectPlayBtn = new Rectangle(menu_left_x, menuIconsRect.getY() + menuIconsRect.getHeight(),
                SCREEN_WIDTH - ic_x, SCREEN_HEIGHT - (menu_top_y + 5 * (ic_size + 5)), vbom);
        rectPlayBtn.setColor(0.9450980392156862f, 0.9725490196078431f, 0.9137254901960784f, 0.4f);

        menuPlay.setScale(rectPlayBtn.getWidth() * 5f / 8f / menuPlay.getHeight());
        //
        btn_play_x = rectPlayBtn.getX() + (rectPlayBtn.getWidth() - menuPlay.getWidth()) / 2f;
        //

        menuPlay.setPosition(btn_play_x,
                rectPlayBtn.getY() + (rectPlayBtn.getHeight() - menuPlay.getHeight()) / 2f);

        menuPlay.setX(menuPlay.getX() + SCREEN_WIDTH - menu_left_x);
        rectPlayBtn.setX(SCREEN_WIDTH);

        gameHud.attachChild(rectPlayBtn);
        gameHud.attachChild(menuPlay);

        //for next turorial ----------------------------
        paranga = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, vbom);
        paranga.setColor(0f,0f,0f,0f);
        gameHud.attachChild(paranga);
        //----------------------------------------------

        //Social nets buttons
        /*
          vk
          g+
          tw
          fb
         */
        //icMenu[4] - groups btn
        icGroups = new mButtonSprite[4];
        origin_groups_x = menu_left_x - ic_size;
        origin_groups_y = new float[icGroups.length];
        for (int i = 0; i < icGroups.length; ++i) {

            final ITextureRegion texture;
            final Buttons btn;

            switch (i) {
                case 0:
                    texture = resourcesManager.ic_fb;
                    btn = Buttons.FB_GROUP;
                    break;
                case 1:
                    texture = resourcesManager.ic_tw;
                    btn = Buttons.TW_GROUP;
                    break;
                case 2:
                    texture = resourcesManager.ic_g;
                    btn = Buttons.G_GROUP;
                    break;
                case 3: default:
                    texture = resourcesManager.ic_vk;
                    btn = Buttons.VK_GROUP;
                    break;
            }

            origin_groups_y[i] = icMenu[4].getY() - i * ic_size;
            icGroups[i] = new mButtonSprite(origin_groups_x, origin_groups_y[i], texture, vbom) {
                @Override
                public void onClicked() {
                    onClick(btn);
                }
            };
        }

        for (mButtonSprite ic : icGroups) {
            ic.setSize(ic_size, ic_size);
            ic.setVisible(false);
            gameHud.attachChild(ic);
        }
        //----------------------------------//

        camera.setHUD(gameHud);
    }

    private void createLogic() {
        smallRectHeight = SCREEN_HEIGHT_NINTH/12f;

        //light column visible if you tap
        column = new Rectangle(0,0, SCREEN_WIDTH/4, SCREEN_HEIGHT, vbom);
        this.attachChild(column);
        column.setVisible(false);

        FallingRect[] rects = new FallingRect[36];
        for (int i = 0; i < rects.length; ++i) {
            rects[i] = new FallingRect(SCREEN_WIDTH, SCREEN_HEIGHT, vbom);
//            rects[i].setVisible(false);

            this.attachChild(rects[i]);
        }

        //hint color below
        bottomColumn = new Rectangle(0,0, SCREEN_WIDTH/4, smallRectHeight, vbom);
        this.attachChild(bottomColumn);
        bottomColumn.setVisible(false);

        startRect = new Rectangle(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT_NINTH/4, vbom);
        this.attachChild(startRect);
        startRect.setVisible(false);

        GameLogic.newInstance(activity, SCREEN_WIDTH, SCREEN_HEIGHT, vbom);
        logic = GameLogic.getInstance();
        difficulty = logic.difficulty = sp.getInt(SP_LAST_DIFFICULTY, 0);
        logic.setIsSound(isSound);
        logic.setSounds(resourcesManager.sound);
        logic.setColors(setColorArray(sp.getInt(SP_CURRENT_COLOR_ARRAY, 0)));
        logic.setRects(rects, startRect, bottomColumn, px_per_inch);

        //btnPause fixes with coordinates
        btn_pause_left = SCREEN_WIDTH - btnPause.getWidth() * 1.5f - 5f;

        btnPause.setPosition(btn_pause_left, startRect.getHeight() + (float)px_per_inch/18f);
    }

    private void createSettings() {
        ic_settings = new Entity[11];

        settings_margin_left = (float)px_per_inch / 15f;

        ic_settings[0] = new Text(settings_margin_left, menuIconsRect.getY(), resourcesManager.font_title,
                resources.getText(R.string.difficulty).toString(), vbom);

        ic_settings[1] = new Rectangle(settings_margin_left, ic_settings[0].getY() + ((Text)ic_settings[0]).getHeight() + 5,
                (float)px_per_inch * 3f / 2f, (float)px_per_inch / 2f, vbom);

        ic_settings[2] = new Rectangle(settings_margin_left, ic_settings[1].getY(),
                (float)px_per_inch / 2f, (float)px_per_inch / 2f, vbom);

        ic_settings[3] = new Text(settings_margin_left, ic_settings[2].getY() + ((Rectangle)ic_settings[2]).getHeight() + 5, resourcesManager.font_title,
                resources.getText(R.string.high_score).toString(), vbom);

        ic_settings[4] = new Text(settings_margin_left, ic_settings[3].getY() + ((Text)ic_settings[3]).getHeight() + 5, resourcesManager.font_middle,
                resources.getText(R.string.difficulty_easy).toString() + "1234567890", vbom);

        ic_settings[5] = new Text(settings_margin_left, ic_settings[4].getY() + ((Text)ic_settings[4]).getHeight() + 5, resourcesManager.font_middle,
                resources.getText(R.string.difficulty_medium).toString() + "1234567890", vbom);

        ic_settings[6] = new Text(settings_margin_left, ic_settings[5].getY() + ((Text)ic_settings[5]).getHeight() + 5, resourcesManager.font_middle,
                resources.getText(R.string.difficulty_hard).toString() + "1234567890", vbom);

        //buttons

        ic_settings[7] = new Rectangle(200, SCREEN_HEIGHT - (float)px_per_inch - adView.getHeight(), 600, (float)px_per_inch/3f, vbom) {
            private boolean wasTouched = false;
            private final Color default_color = new Color(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f),
                    pressed_color = new Color(0.8f, 0.8f, 0.8f, 0.6f);//0.93 and 0.33
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    wasTouched = true;
                    registerEntityModifier(new ColorModifier(0.1f, default_color, pressed_color));
                } else if (pSceneTouchEvent.isActionUp()) {
                    if (wasTouched) {
                        this.setColor(default_color);
                        onClick(Buttons.TUTORIAL);
                    }
                    wasTouched = false;
                }
                return true;
            }
        };
        ic_settings[8] = new Rectangle(200, ic_settings[7].getY() + ((Rectangle)ic_settings[7]).getHeight() + 20, 600, (float)px_per_inch/3f, vbom) {
            private boolean wasTouched = false;
            private final Color default_color = new Color(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f),
                    pressed_color = new Color(0.8f, 0.8f, 0.8f, 0.6f);//0.93 and 0.33

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    wasTouched = true;
                    registerEntityModifier(new ColorModifier(0.1f, default_color, pressed_color));
                } else if (pSceneTouchEvent.isActionUp()) {
                    if (wasTouched) {
                        registerEntityModifier(new ColorModifier(0.1f, pressed_color, default_color));
                        onClick(Buttons.REPORT);
                    }
                    wasTouched = false;
                }
                return true;
            }
        };

        ic_settings[9] = new Text(0, 0, resourcesManager.font, resources.getString(R.string.tutorial), vbom);
        ic_settings[10] = new Text(0, 0, resourcesManager.font, resources.getString(R.string.report_bug), vbom);
        updateTextX((Text)ic_settings[9]);
        updateTextX((Text)ic_settings[10]);

        ic_settings[9].setY(
                ic_settings[7].getY() + (((Rectangle) ic_settings[7]).getHeight() - ((Text) ic_settings[9]).getHeight()) / 2);

        ic_settings[10].setY(
                ic_settings[8].getY() + (((Rectangle) ic_settings[8]).getHeight() - ((Text) ic_settings[10]).getHeight()) / 2);

        //for buttons to be aligned
        final float maxMargin = Math.min(ic_settings[9].getX(), ic_settings[10].getX());
        ic_settings[7].setX(maxMargin - settings_margin_left);
        ic_settings[8].setX(maxMargin - settings_margin_left);
        ((Rectangle)ic_settings[7]).setWidth(SCREEN_WIDTH - maxMargin - ic_settings[7].getX() + settings_margin_left);
        ((Rectangle)ic_settings[8]).setWidth(SCREEN_WIDTH - maxMargin - ic_settings[8].getX() + settings_margin_left);

        settings_x_buttons = ic_settings[7].getX();
        settings_text_tutorial = ic_settings[9].getX();
        settings_text_report = ic_settings[10].getX();


        ic_settings[1].setColor(.7f, .7f, .7f, .4f);
        ic_settings[2].setColor(0f, 0f, 0f, 0.3f);
        ic_settings[7].setColor(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f);
        ic_settings[8].setColor(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f);

        ((Rectangle)ic_settings[2]).setWidth((sp.getInt(SP_LAST_DIFFICULTY, 0) + 1) * (float) px_per_inch / 2f);

        ((Text) ic_settings[4]).setText(resources.getString(R.string.difficulty_easy) + " " + sp.getInt(SP_HIGH_SCORE[0], 0));
        ((Text)ic_settings[5]).setText(resources.getString(R.string.difficulty_medium) + " " + sp.getInt(SP_HIGH_SCORE[1], 0));
        ((Text)ic_settings[6]).setText(resources.getString(R.string.difficulty_hard) + " " + sp.getInt(SP_HIGH_SCORE[2], 0));

        for (Entity ic : ic_settings) {
            ic.setX(- SCREEN_WIDTH);
            gameHud.attachChild(ic);
        }

        /*
        Text: Dif
        Dif bar
        High Score:
        3 score ints
        Tutorial btn
        Report btn
        */
    }

    private void onChangeDifficulty(final int dif) {
        if (isDifficultyChanging) return;

        // Timer is Width modifier as I can't change width with andengine resources
        isDifficultyChanging = true;

        ic_settings[2].registerEntityModifier(new SingleValueSpanEntityModifier(.6f,
                ((Rectangle)ic_settings[2]).getWidth(), (float)px_per_inch / 2f * (dif + 1)) {
            protected void onSetInitialValue(IEntity pItem, float pValue) {}
            @Override
            protected void onSetValue(IEntity pItem, float pPercentageDone, float pValue) {
                ((Rectangle)pItem).setWidth(pValue);
                if (pPercentageDone >= 1f) isDifficultyChanging = false;
            }
            public IEntityModifier deepCopy() throws DeepCopyNotSupportedException {return null;}
        });


        if (sp.getInt(SP_LAST_DIFFICULTY, 0) != dif) {
            sp.edit().putInt(SP_LAST_DIFFICULTY, dif).apply();
            difficulty = dif;
            if (wasGamePlay)
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, resources.getString(R.string.change_difficulty_after_restart), Toast.LENGTH_SHORT).show();
                    }
                });
            else
                logic.difficulty = dif;
        }
    }

    private void createGameOver() {
        gameOverScreen = new Entity[16];

        final float home_btn_height = (float) (px_per_inch / 1.8);

        gameOverScreen[0] = new Rectangle(0, (float) (SCREEN_HEIGHT - px_per_inch), SCREEN_WIDTH, home_btn_height, vbom) {
            private boolean wasTouched = false;
            private final Color default_color = new Color(1f, 1f, 1f, 0.2f),
                    pressed_color = new Color(0.8f, 0.8f, 0.8f, 0.3f);//0.93 and 0.33

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    wasTouched = true;
                    registerEntityModifier(new ColorModifier(0.1f, default_color, pressed_color));
                } else if (pSceneTouchEvent.isActionUp()) {
                    if (wasTouched) {
                        onClick(Buttons.GAMEOVER);
                        registerEntityModifier(new ColorModifier(0.1f, pressed_color, default_color));
                    }
                    wasTouched = false;
                }
                return true;
            }
        };
        gameOverScreen[0].setColor(1f, 1f, 1f, 0.2f);

        gameOverScreen[1] = new Sprite(
                0, 0,
                ((Rectangle) gameOverScreen[0]).getHeight()/* - (float) px_per_inch*2/5*/,
                ((Rectangle) gameOverScreen[0]).getHeight()/*- (float) px_per_inch*2/5*/,
                resourcesManager.ic_home_game_over, vbom);
        gameOverScreen[1].setPosition((SCREEN_WIDTH - ((Sprite) gameOverScreen[1]).getWidth()) / 2,
                gameOverScreen[0].getY()/* + (float) px_per_inch / 5*/);


        gameOverScreen[2] = new mButtonSprite(0, 0, resourcesManager.ic_fb, vbom) {
            @Override
            public void onClicked() {
                onClick(Buttons.FB_SHARE);
            }
        };
        gameOverScreen[3] = new mButtonSprite(0, 0, resourcesManager.ic_tw, vbom) {
            @Override
            public void onClicked() {
                onClick(Buttons.TW_SHARE);
            }
        };
        gameOverScreen[4] = new mButtonSprite(0, 0, resourcesManager.ic_g, vbom) {
            @Override
            public void onClicked() {
                onClick(Buttons.G_SHARE);
            }
        };
        gameOverScreen[5] = new mButtonSprite(0, 0, resourcesManager.ic_vk, vbom) {
            @Override
            public void onClicked() {
                onClick(Buttons.VK_SHARE);
            }
        };

        final float overall_ic_width = home_btn_height < SCREEN_WIDTH_QUARTER ?
                home_btn_height * 4f / 1.25f :
                SCREEN_WIDTH;

        final float margin_left = (SCREEN_WIDTH - overall_ic_width) / 2f,
                ic_size = overall_ic_width / 4f;

        for (int i = 2; i < 6; ++i) {
            ((mButtonSprite) gameOverScreen[i]).setSize(ic_size, ic_size);
            gameOverScreen[i].setPosition(margin_left + ic_size * (i - 2),
                    gameOverScreen[0].getY() - ic_size);
        }

        //----------------------- TEXTS FOR SCORES AT GAME OVER ---------------------------//

        //size gets from {Y} of menu rect to {Y} of share buttons with margins of about 1 sm
//        final float text_max_size = gameOverScreen[2].getY() - menuIconsRect.getY()
//                - (float) px_per_inch / 2.5f;

//            text_height = text_max_size / ((float) px_per_inch / 2.5f + margin_text) >= 7 ?
//                    (float) px_per_inch / 2.5f :
//                    text_max_size / (7 + margin_text);

        final float margin_text = 5;//px between texts

        gameOverScreen[6] = new Text(0, menuIconsRect.getY(), resourcesManager.font_title,
                resources.getText(R.string.game_over).toString(), vbom);
        updateTextX((Text) gameOverScreen[6]);

        gameOverScreen[7] = new Text(0, gameOverScreen[6].getY() + ((Text) gameOverScreen[6]).getHeight() + margin_text * 2,
                resourcesManager.font_middle, "Difficulty: easy b", 20, vbom);
        gameOverScreen[8] = new Text(0, gameOverScreen[7].getY() + ((Text) gameOverScreen[7]).getHeight() + margin_text,
                resourcesManager.font_middle, "Score: 123456789", 20, vbom);
        gameOverScreen[9] = new Text(0, gameOverScreen[8].getY() + ((Text) gameOverScreen[8]).getHeight() + margin_text,
                resourcesManager.font_middle, "Last Score: 123456789", 23, vbom);
        gameOverScreen[10] = new Text(0, gameOverScreen[9].getY() + ((Text) gameOverScreen[9]).getHeight() + margin_text,
                resourcesManager.font_middle, "High Score: 123456789", 23, vbom);
        gameOverScreen[11] = new Text(0, gameOverScreen[10].getY() + ((Text) gameOverScreen[10]).getHeight() + margin_text,
                resourcesManager.font_middle, "Money Got: 123456789", 22, vbom);

        gameOverScreen[12] = new Text(0, gameOverScreen[2].getY() - ((Text) gameOverScreen[7]).getHeight(),
                resourcesManager.font, resources.getText(R.string.share).toString(), vbom);
        updateTextX((Text) gameOverScreen[12]);

        //set x like at high score text
        updateTextX((Text) gameOverScreen[10]);
        for (int i = 7; i < 12; ++i)
            gameOverScreen[i].setX(gameOverScreen[10].getX());

        //if distance between last text of score stats and hint text is more than half inch, do:
        if (gameOverScreen[12].getY() - gameOverScreen[11].getY() > (float) px_per_inch * 5f / 12f) {
            final float shift_dist = gameOverScreen[12].getY() - (float) px_per_inch * 5f / 12f - gameOverScreen[11].getY();
            for (int i = 6; i < 12; ++i) {
                gameOverScreen[i].setY(gameOverScreen[i].getY() + shift_dist);
            }
        }

        //ToDo: what if text is smaller than the icon
        gameOverScreen[13] = new Sprite(gameOverScreen[11].getX() + ((Text) gameOverScreen[11]).getWidth() + 3f,
                gameOverScreen[11].getY() + (((Text) gameOverScreen[11]).getHeight() - resourcesManager.ic_money.getHeight()) / 2f,
                resourcesManager.ic_money, vbom);
        gameOverScreen[13].setY(gameOverScreen[13].getY() +
                (((Sprite) gameOverScreen[13]).getHeight() - ((Sprite) gameOverScreen[13]).getHeight() * .75f) / 2f);
        ((Sprite)gameOverScreen[13]).setSize(((Sprite) gameOverScreen[13]).getWidth() * .75f,
                ((Sprite) gameOverScreen[13]).getHeight() * .75f);

        //this sprite is created to be scaled
        gameOverScreen[14] = new Sprite(0f, 0f,
                resourcesManager.ic_money, vbom);
        final float sizeDivider = ((Text)gameOverScreen[12]).getHeight() / ((Sprite)gameOverScreen[14]).getHeight();
        ((Sprite)gameOverScreen[14]).setSize(((Sprite) gameOverScreen[14]).getWidth() * sizeDivider,
                ((Sprite) gameOverScreen[14]).getHeight() * sizeDivider);
        gameOverScreen[14].setPosition(gameOverScreen[12].getX() + ((Text) gameOverScreen[12]).getWidth() + 5f,
                gameOverScreen[12].getY());

        //ToDo: check scaling on big screen and small screen
        gameOverScreen[15] = new Sprite((SCREEN_WIDTH - resourcesManager.ic_cup.getWidth()) / 2f,
                gameOverScreen[6].getY() > resourcesManager.ic_cup.getHeight() * 2f ?
                        gameOverScreen[6].getY() - resourcesManager.ic_cup.getHeight() - (float) px_per_inch / 20f :
                        (gameOverScreen[6].getY() - resourcesManager.ic_cup.getHeight()) / 2f,
                resourcesManager.ic_cup, vbom);

        //---------------------------------------------------------------------------------//


        for (final Entity e : gameOverScreen)
            this.attachChild(e);

        setVisGameOver(false);
    }

    private void startTutorialSubscribe() {

        final Rectangle answerRect;
        final Text helpText, buttonText;
        final Sprite icMoney;

        final float margin_text = (float)px_per_inch / 20f;


        helpText = new Text(0f, 0f, resourcesManager.font, activity.getText(R.string.tutorial_groups_menu), vbom);
        buttonText = new Text(0f, 0f, resourcesManager.font, activity.getText(R.string.tutorial_groups_button), vbom);
        icMoney = new Sprite(0f, 0f, resourcesManager.ic_money, vbom);
        answerRect = new Rectangle(0f,0f,0f,0f,vbom);
        buttonRect = new Rectangle(0f,0f,0f,0f,vbom) {
            private boolean wasTouched = false;

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionMove()) {
                    if (pTouchAreaLocalX >= getX() + getWidth() || pTouchAreaLocalX <= getX()
                            || pTouchAreaLocalY >= getY() + getHeight() || pTouchAreaLocalY <= getY())
                        this.setColor(ColorArray.green4[0], ColorArray.green4[1], ColorArray.green4[2]);
                    else
                        this.setColor(ColorArray.green3[0], ColorArray.green3[1], ColorArray.green3[2]);

                }
                if (pSceneTouchEvent.isActionDown()) {
                    wasTouched = true;
                    this.setColor(ColorArray.green4[0], ColorArray.green4[1], ColorArray.green4[2]);
                } else if (pSceneTouchEvent.isActionUp() && wasTouched) {
                    wasTouched = false;
                    this.setColor(ColorArray.green3[0], ColorArray.green3[1], ColorArray.green3[2]);

                    ((GameActivity) activity).runOnUpdateThread(new Runnable() {
                        @Override
                        public void run() {
                            setGroupsVis(false);
                        }
                    });

                    for (final Entity entity : new Entity[]{paranga, answerRect, this, helpText, buttonText, icMoney} )
                        entity.registerEntityModifier(new AlphaModifier(.6f, entity.getAlpha(), 0f,
                                new IEntityModifier.IEntityModifierListener() {
                                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                                    @Override
                                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                        ((GameActivity) activity).runOnUpdateThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                gameHud.unregisterTouchArea((ITouchArea) entity);
                                                entity.detachSelf();
                                                entity.dispose();
                                                paranga.setSize(0f, 0f);
                                                paranga.setVisible(false);
                                            }
                                        });
                                    }
                                }));

                    //startTutorialSubscribe
                    gameHud.unregisterTouchArea(this);
                }

                return true;
            }
        };

        buttonRect.setSize(helpText.getWidth(), buttonText.getHeight() + margin_text * 2f);

        answerRect.setSize(helpText.getWidth() + margin_text * 2f,
                helpText.getHeight() + margin_text * 3f + buttonRect.getHeight());

        helpText.setPosition(origin_groups_x - helpText.getWidth() - margin_text * 2f,
                origin_groups_y[3] + margin_text * 2f);
        answerRect.setPosition(helpText.getX() - margin_text, helpText.getY() - margin_text);
        buttonRect.setPosition(helpText.getX(), helpText.getY() + helpText.getHeight() + margin_text);
        buttonText.setPosition(buttonRect.getX() + (buttonRect.getWidth() - buttonText.getWidth())/2f,
                buttonRect.getY() + (buttonRect.getHeight() - buttonText.getHeight())/2f);


        answerRect.setColor(1f,1f,1f,0f);
        buttonRect.setColor(ColorArray.green3[0], ColorArray.green3[1], ColorArray.green3[2], 0f);
        helpText.setAlpha(0f);
        buttonText.setAlpha(0f);
        icMoney.setAlpha(0f);

        final float divider = helpText.getHeight() / 3f / icMoney.getHeight();
        icMoney.setSize(icMoney.getWidth() * divider, icMoney.getHeight() * divider);
        //sets position to the right of line "and get 200.."
        icMoney.setPosition(helpText.getX() + helpText.getLineWidths().get(1), helpText.getY() + helpText.getHeight() / 3f + (helpText.getHeight() / 3f - resourcesManager.ic_money.getHeight() * divider) / 2f);

        gameHud.registerTouchArea(buttonRect);
        gameHud.attachChild(answerRect);
        gameHud.attachChild(buttonRect);
        gameHud.attachChild(helpText);
        gameHud.attachChild(buttonText);
        gameHud.attachChild(icMoney);

//        onClick(Buttons.GROUPS);
        setAlphaAnim(paranga, .4f, true);
        setAlphaAnim(answerRect, .7f, true);
        setAlphaAnim(buttonRect, .7f, true);
        setAlphaAnim(helpText, 1f, true);
        setAlphaAnim(buttonText, 1f, true);
        setAlphaAnim(icMoney, 1f, true);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((GameActivity)activity).runOnUpdateThread(new Runnable() {
                            @Override
                            public void run() {
                                setGroupsVis(true);
                            }
                        });
                    }
                }, 500);
            }
        });
    }

    private void setAlphaAnim(final Entity e, final float toAlpha, final boolean fromMin) {
        e.registerEntityModifier(new DelayModifier(1f, new IEntityModifier.IEntityModifierListener() {
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                e.registerEntityModifier(new AlphaModifier(.3f, fromMin ? 0f : 1f, toAlpha));
            }
        }));
    }

    private void setVisGameOver(boolean visibility) {
        isGameOverScreenOut = visibility;
        for (final Entity e : gameOverScreen)
            e.setVisible(visibility);
        if (visibility) {
            gameHud.registerTouchArea((Rectangle) gameOverScreen[0]);
            for (int i = 2; i < 6; ++i)
                gameHud.registerTouchArea((mButtonSprite) gameOverScreen[i]);
            if (!logic.newScore) gameOverScreen[15].setVisible(false);
        } else {
            gameHud.unregisterTouchArea((Rectangle) gameOverScreen[0]);
            for (int i = 2; i < 6; ++i)
                gameHud.unregisterTouchArea((mButtonSprite)gameOverScreen[i]);
        }
    }

    private void onPause() {
        if (!isPaused) {
            togglePause();
            moveToLeft(menuIconsRect, menu_left_x);
            setMenuVis(true);
            moveToLeft(menuMoney, left_menu_money_ic);
            moveToLeft(moneyText, getMoneyTextX());
        }
    }

    private void breakAfterPauseTimer() {
        setMenuVis(true);
        moveToLeft(menuIconsRect, menu_left_x);
        moveToLeft(menuMoney, left_menu_money_ic);
        moveToLeft(moneyText, getMoneyTextX());

        afterPauseCounter.unregisterUpdateHandler(afterPauseTimer);
        afterPauseTimer.setAutoReset(false);
        afterPauseTimer = null;

        afterPauseSeconds = 3;
        afterPauseCounter.setText("3");
        updateTextX(afterPauseCounter);
        afterPauseCounter.setVisible(false);
        isTimerPause = false;

    }

    @Override
    public void onBackKeyPressed() {
        if (doubleBackPress) System.exit(0);

        else if (logic.isGameOver()) {
            onClick(Buttons.GAMEOVER);     }

        else if (!isPaused) onClick(Buttons.PAUSE);

        else {
            doubleBackPress = true;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, R.string.click_back_again, Toast.LENGTH_SHORT).show();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackPress = false;
                }
            }, 1000);

            if (isTimerPause) breakAfterPauseTimer();
        }
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_MENU;
    }

    @Override
    public void disposeScene() {}

    // -------------------------
    // -------------------------

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);
        logic.update();
        if (!logic.rects[logic.RECT_I_WORK_WITH].drawBgColumn) {
            column.setVisible(false);
            bottomColumn.setVisible(false);
        }
        if (logic.scoreChanged) {
            scoreText.setText(logic.score + "");
            updateTextX(scoreText);
            logic.scoreChanged = false;

            logic.currentLevel = logic.score >= 500 ?
                    (logic.score - (logic.score % 500)) / 500 + 1 : 1;
            if (logic.wasLevel != logic.currentLevel) {
                levelText.setText(resources.getString(R.string.level) + " " + logic.currentLevel);
                logic.wasLevel = logic.currentLevel;
            }
        }
        if (logic.isGameOver() && !wasGameOverDataUpdated) {
            //ToDo: anim for ads, blocks, score

            wasGameOverDataUpdated = true;

            for (FallingRect r : logic.rects)
                    r.registerEntityModifier(new AlphaModifier(.4f, 1f, .45f));
            bottomColumn.registerEntityModifier(new AlphaModifier(.4f, 1f, .45f));
            startRect.registerEntityModifier(new AlphaModifier(.4f, 1f, .45f));

            logic.startGameOverAnimation();

            //after gameOver btnPause is invisible
            btnPause.setVisible(false);
            btnPause.setX(SCREEN_WIDTH);

            //ToDo: later change to animation
            scoreText.setVisible(false);
            levelText.setVisible(false);

            //plus money (10 p = 1 coin)
            gotMoney = logic.score / 10;
            final int dif = logic.difficulty;

            // Adding money
            sp.edit().putInt(SP_MONEY, sp.getInt(SP_MONEY, 0) + gotMoney).apply();

            final int last_score = sp.getInt(SP_LAST_SCORE[dif], logic.score);

            SharedPreferences.Editor editor = sp.edit();

            //ic_cup sets in setVisGameOver
            if (sp.getInt(SP_HIGH_SCORE[dif], 0) < logic.score) {
                editor.putInt(SP_HIGH_SCORE[dif], logic.score);
                logic.newScore = true;
            }
            editor.putInt(SP_LAST_SCORE[dif], logic.score);
            editor.apply();
            final int high_score = sp.getInt(SP_HIGH_SCORE[dif], logic.score);


            final String difStr;
            switch (difficulty) {
                case 0: default:
                    difStr = resources.getString(R.string.difficulty_easy);
                    break;
                case 1:
                    difStr = resources.getString(R.string.difficulty_medium);
                    break;
                case 2:
                    difStr = resources.getString(R.string.difficulty_hard);
            }

            ((Text)gameOverScreen[7]).setText(resources.getString(R.string.difficulty) + ": " + difStr);
            ((Text)gameOverScreen[8]).setText(resources.getString(R.string.score) + ": " + logic.score);
            ((Text)gameOverScreen[9]).setText(resources.getString(R.string.last_score) + ": " + last_score);
            ((Text)gameOverScreen[10]).setText(resources.getString(R.string.high_score) + ": " + high_score);
            ((Text)gameOverScreen[11]).setText(resources.getString(R.string.got) + " " + gotMoney);
            moneyText.setText(sp.getInt(SP_MONEY, 0) + "");

            gameOverScreen[13].setX(gameOverScreen[11].getX() + ((Text) gameOverScreen[11]).getWidth() + 3);

            setVisGameOver(true);
            wasGamePlay = false;

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    // ----------------------------------------------------//
    // ------------------ TOUCH EVENTS --------------------//
    // ----------------------------------------------------//

    public void onClick(final Buttons btn) {
        if (paranga.isVisible() && !(btn == Buttons.FB_GROUP || btn == Buttons.G_GROUP
                || btn == Buttons.TW_GROUP || btn == Buttons.VK_GROUP)) return;

        final String url_to_go;

        //had to make fb code like an idiot as it sent analytics always
        switch (btn) {
            case FB_GROUP:
                ShadesApplication.getInstance().trackEvent(shareString, openGroupString, fbString); default:
                url_to_go = "https://www.facebook.com/cappable";

                addMoneyForFollow(fb_id);
                break;
            case VK_GROUP:
                ShadesApplication.getInstance().trackEvent(shareString, openGroupString, vkString);
                url_to_go = "http://vk.com/cappable";

                addMoneyForFollow(vk_id);
                break;
            case TW_GROUP:
                ShadesApplication.getInstance().trackEvent(shareString, openGroupString, twString);
                url_to_go = "https://twitter.com/cappableApps";

                addMoneyForFollow(tw_id);
                break;
            case G_GROUP:
                ShadesApplication.getInstance().trackEvent(shareString, openGroupString, googleString);
                url_to_go = "https://plus.google.com/103646614447789437593/about";

                addMoneyForFollow(go_id);
                break;
        }

        switch(btn) {
            case PAUSE:
                onPause();
                break;

            case SETTINGS:
                if (!isSettingsOut) {
                    if (isShopOut) setShopVis(false);
                    if (helpText.isVisible()) helpText.setVisible(false);
                    setSettingsVis(true);
                    setBtnPlayVis(true);
                    setGroupsVis(false);
                }
                break;

            case SOUND:
                toggleSound();
                break;

            case SHOP:
                if (!isShopOut) {
                    if (helpText.isVisible()) helpText.setVisible(false);
                    if (isSettingsOut) setSettingsVis(false);
                    setBtnPlayVis(true);
                    setGroupsVis(false);
                    setShopVis(true);
//                    if (appnameVisible) ivAppName.startAnimation(appnameAlphaToMin);
                }
                break;

            case RESTART:
                if (doubleRestartPress || !isGameplay) {
                    if (isPaused) togglePause();
                    if (isShopOut) setShopVis(false);
                    if (isSettingsOut) setSettingsVis(false);
                    setGroupsVis(false);

                    setColorArray(shopFragment.currentColor);
                    logic.difficulty = sp.getInt(SP_LAST_DIFFICULTY, 0);
                    logic.restart(Colors);
                    setBackground(new Background(new Color(Colors[6][0], Colors[6][1], Colors[6][2])));
                    startGame();
                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, resources.getString(R.string.click_restart_again), Toast.LENGTH_SHORT).show();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doubleRestartPress = false;
                                }
                            }, 1500);
                        }
                    });

                    doubleRestartPress = true;
                }

                break;

            case GROUPS:
                //HERE Code
                setGroupsVis(true);
                setSettingsVis(false);
                setShopVis(false);
                break;
            case VK_GROUP:
            case FB_GROUP:
            case TW_GROUP:
            case G_GROUP:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url_to_go));
                activity.startActivity(i);
                break;

            case VK_SHARE:
            case FB_SHARE:
            case TW_SHARE:
            case G_SHARE:
                onShareRecord(btn);
                break;

            case GAMEOVER:
                wasGamePlay = false;
                wasGameOverDataUpdated = false;


                for (FallingRect r : logic.rects)
                    r.setAlpha(1f);
                bottomColumn.setAlpha(1f);
                startRect.setAlpha(1f);

                //ToDo: change to animations
                setVisGameOver(false);
                levelText.setVisible(true);
                scoreText.setVisible(true);

                logic.reset(Colors);
                setBackground(new Background(new Color(Colors[6][0], Colors[6][1], Colors[6][2])));
                onClick(Buttons.PAUSE);

                wasSharedInThisGame = 0;
                extraMoney = 0;
                break;

            case TUTORIAL:
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adView.setVisibility(View.GONE);
                        //for not happening bug. When after restart of tutorial, text is old.
                    }
                });

                SceneManager.getInstance().createTutorialScene(true);
                final TutorialScene scene = (TutorialScene)SceneManager.getInstance().getCurrentScene();
                scene.registerUpdateHandler(new IUpdateHandler() {
                    @Override
                    public void onUpdate(float pSecondsElapsed) {
                        if (scene.isEnd()) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adView.setVisibility(View.VISIBLE);
                                }
                            });
                            SceneManager.getInstance().disposeTutorialScene();

                            camera.setHUD(gameHud);

                            //as it doesn't toggle back from tutorial
                            ic_settings[7].setColor(new Color(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f));
                            ic_settings[7].registerEntityModifier(new ColorModifier(0.1f, ic_settings[7].getColor(),
                                    new Color(0.9333333333333333f, 0.9333333333333333f, 0.9333333333333333f, 0.3333333333333333f)));

                        }
                    }
                    public void reset() {}
                });
                break;
            case REPORT:
                Intent bugReport = new Intent(Intent.ACTION_SEND);
                bugReport.setType("message/rfc822");
                bugReport.putExtra(Intent.EXTRA_EMAIL, new String[]{"cappableApps@gmail.com"});
                bugReport.putExtra(Intent.EXTRA_SUBJECT, "Found Bug");
                bugReport.putExtra(Intent.EXTRA_TEXT, "");
                activity.startActivity(Intent.createChooser(bugReport, "Send Email"));
                break;
        }
    }

    @Override
    public boolean onSceneTouchEvent(TouchEvent pSceneTouchEvent) {
        final float x = pSceneTouchEvent.getX();
        final float y = pSceneTouchEvent.getY();

        final int screenPart = (int) (x / SCREEN_WIDTH_QUARTER);

        if (btnPause.contains(x,y) || isTimerPause ||
                screenPart > 3 || screenPart < 0 || (paranga.isVisible()
                && (!icGroups[0].contains(x,y) && !icGroups[1].contains(x,y)
                && !icGroups[2].contains(x,y) && !icGroups[1].contains(x,y) || buttonRect.contains(x,y)))) return false;
        // 2 last conditions needed just to avoid ArrayIndexOutOfBoundsException
        // as emulator threw it when I touched out of the screen :)))

        if (!isPaused)
            switch (pSceneTouchEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (slideTouchLocker) {
                        slideTouchLocker = false;
                        wasXTouch = x;
                        wasYTouch = y;
                    }
                case MotionEvent.ACTION_MOVE:
                    if (logic.getRectIWorkWith().isOKtoChangeColumn) {
                        logic.getRectIWorkWith().setColumn(screenPart + 1);
                    }

                    //drawing background column
                    column.setPosition(SCREEN_WIDTH_QUARTER * screenPart, 0);
                    if (logic.getRectIWorkWith().drawBgColumn) {
                        column.setVisible(true);

                        bottomColumn.setPosition(SCREEN_WIDTH_QUARTER * screenPart, SCREEN_HEIGHT - SCREEN_HEIGHT_NINTH * logic.upper[screenPart] - smallRectHeight);
                        bottomColumn.setVisible(true);
                    }

                    if (wasYTouch < y - px_per_inch*3f/4f) {
                        logic.getRectIWorkWith().setFallingFaster();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    bottomColumn.setVisible(false);
                    column.setVisible(false);
                    wasXTouch = wasYTouch = SCREEN_HEIGHT;
                    slideTouchLocker = true;
                    break;
            }
        else {
            //ToDo: delete isPlaying later maybe

            if (isSettingsOut && (((Rectangle)ic_settings[7]).contains(x,y) || ((Rectangle)ic_settings[8]).contains(x,y)) ||
                    ((Rectangle)ic_settings[1]).contains(x,y) || ((Rectangle)ic_settings[2]).contains(x,y)) {
                if (pSceneTouchEvent.isActionUp()) {
                //
                    if (((Rectangle)ic_settings[7]).contains(x,y)) onClick(Buttons.TUTORIAL);
                    else if (((Rectangle)ic_settings[8]).contains(x,y)) onClick(Buttons.REPORT);
                    else if (((Rectangle)ic_settings[1]).contains(x,y))
                        onChangeDifficulty((int) ((x - ic_settings[1].getX())
                                                        / ((float)px_per_inch / 2f)));
//                    else if (((Rectangle)ic_settings[3]).contains(x,y)) onChangeDifficulty(2);

//                } else if (pSceneTouchEvent.isActionDown()) {
//                //ToDo animations
                }
            }
            //screen touch to start
            //check for not turning on the game after pause touch click
            else if (pSceneTouchEvent.isActionUp()) {
                if (!isOKtoClickScreen) isOKtoClickScreen = true;
                else if (!menuIconsRect.contains(x, y) && !(isGameOverScreenOut && ((Rectangle) gameOverScreen[0]).contains(x, y))) {
                    if (!wasGamePlay) {
                        continueGame();
                    }
                    else {
                        afterPauseCounter.setText("3");
                        updateTextX(afterPauseCounter);
                        afterPauseCounter.setVisible(true);
                        isTimerPause = true;
                        setMenuVis(false);
                        moveToRight(menuIconsRect);
                        setBtnPlayVis(false);

                        afterPauseCounter.registerUpdateHandler(afterPauseTimer =
                                new TimerHandler(1, true, new ITimerCallback() {
                                    @Override
                                    public void onTimePassed(TimerHandler pTimerHandler) {
                                        if (--afterPauseSeconds > 0) {
                                            afterPauseCounter.setText(afterPauseSeconds + "");
                                        } else {
                                            resetAfterPauseTimerValues();
                                            if (afterPauseTimer != null) {
                                                afterPauseTimer.setAutoReset(false);
                                                afterPauseTimer = null;
                                            }

                                        }
                                    }
                                }));
                    }

                    if (isShopOut) setShopVis(false);
                    else if (isSettingsOut) setSettingsVis(false);
                    setGroupsVis(false);
                }
            }
        }
        return false;
    }

    private void resetAfterPauseTimerValues() {
        afterPauseSeconds = 3;
        afterPauseCounter.unregisterUpdateHandler(afterPauseTimer);
        afterPauseCounter.setText(afterPauseSeconds + "");
        afterPauseCounter.setVisible(false);
        continueGame();
        isTimerPause = false;
    }

    private void continueGame() {
        if (!(wasGamePlay || isBeforeFirstGame )) {
            setColorArray(shopFragment.currentColor);
            logic.difficulty = sp.getInt(SP_LAST_DIFFICULTY, 0);
            logic.restart(Colors);
            setBackground(new Background(new Color(Colors[6][0], Colors[6][1], Colors[6][2])));
        }
        togglePause();
        wasGamePlay = true;
        logic.isPlaying = true;
        isBeforeFirstGame = false;//one time boolean. Not to change color two times
        startGame();
    }

    private void toggleSound() {
        isSound = !isSound;
        ((mToggleButtonSprite) icMenu[sound]).setCurrentTileIndex(isSound ? 1 : 0);
        Log.d("sound: ", "newSound" + isSound);
        sp.edit().putBoolean(SP_SOUND, isSound).commit();
        logic.setIsSound(isSound);
    }

    private void togglePause() {
        //ToDo: later add blur
        final boolean wasPaused = isPaused;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adView.setVisibility(wasPaused ? View.INVISIBLE : View.VISIBLE);
            }
        });
        if (!wasPaused) {
            btnPause.setVisible(false);
            btnPause.setX(SCREEN_WIDTH);
        }
        isPaused = !isPaused;
        logic.setRectStatic(!FallingRect.drawStatic);
    }

    //Method for typical start of game events
    private void startGame() {
        isGameplay = true;
        if (helpText.isVisible())
            helpText.registerEntityModifier(new AlphaModifier(0.25f, 1, 0){
                @Override
                protected void onModifierFinished(IEntity pItem) {
                    super.onModifierFinished(pItem);
                    helpText.setVisible(false);
                }
            });

        btnPause.registerEntityModifier(new MoveXModifier(anim_menu_out_time, SCREEN_WIDTH, btn_pause_left));
        btnPause.setVisible(true);


        moveToRight(menuMoney);
        moveToRight(menuIconsRect);
        setMenuVis(false);
        moveToRight(moneyText);

        setBtnPlayVis(false);
        setGroupsVis(false);
    }

    //---------------------------------------------------------------------------------//
    //---------------------------------------------------------------------------------//
    //------------------------------ SOCIAL NETS HERE ---------------------------------//
    //---------------------------------------------------------------------------------//
    //---------------------------------------------------------------------------------//
    public void onShareRecord(Buttons btn){
        final String difStr;
        switch (logic.difficulty) {
            case 0:
                difStr = resources.getString(R.string.difficulty_easy_share);
                break;
            case 1:
                difStr = resources.getString(R.string.difficulty_medium_share);
                break;
            default:
                difStr = resources.getString(R.string.difficulty_hard_share);
        }

        String title = resources.getString(R.string.shareFirst) + " " + difStr + " " + resources.getString(R.string.shareSecond)
                + " - " + logic.score + resources.getString(R.string.shareThird) + "\n #android #shades2.0 #game";
        String link = "https://play.google.com/store/apps/details?id=com.cappable.shades";
        String msg = resources.getString(R.string.shareFirst) + " " + difStr + " " + resources.getString(R.string.shareSecond)
                + " - " + logic.score + resources.getString(R.string.shareThird) + " %23android %23shades2.0 %23game" + " " + link;
        Intent i;

        switch (btn){
            case TW_SHARE:
                title = resources.getString(R.string.shareFirst) + " " + difStr + " " + resources.getString(R.string.shareSecond)
                        + " - " + logic.score + resources.getString(R.string.shareThird) + " %23android %23shades2.0 %23game";
                i = new Intent(Intent.ACTION_VIEW);
                String url = "http://www.twitter.com/intent/tweet?url=" + link + "&text=" + title;
                i.setData(Uri.parse(url));
                activity.startActivityForResult(i, 0);

                ShadesApplication.getInstance().trackEvent(shareString, shareResultString, twString);

                if (wasSharedInThisGame < 2 && isNetworkAvailable()) {
                    extraMoney += 30;
                    gameOverScreen[11].registerUpdateHandler(new TimerHandler(4, new ITimerCallback() {
                        @Override
                        public void onTimePassed(TimerHandler pTimerHandler) {
                            ((Text)gameOverScreen[11]).setText(resources.getString(R.string.got) + " " + gotMoney + " + " + extraMoney);
                            gameOverScreen[13].setX(gameOverScreen[11].getX() + ((Text) gameOverScreen[11]).getWidth() + 3);
                        }
                    }));
                    ++wasSharedInThisGame;
                    sp.edit().putInt(SP_MONEY, sp.getInt(SP_MONEY, 0) + 30).apply();
                }
                break;
            case G_SHARE:
                try {
                    i = new PlusShare.Builder(activity)
                            .setType("text/plain")
                            .setText(title)
                            .setContentUrl(Uri.parse(link))
                            .getIntent();
                    activity.startActivityForResult(i, 0);

                    ShadesApplication.getInstance().trackEvent(shareString, shareResultString, googleString);

                    if (wasSharedInThisGame < 2 && isNetworkAvailable()) {
                        extraMoney += 30;
                        gameOverScreen[11].registerUpdateHandler(new TimerHandler(4, new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                ((Text) gameOverScreen[11]).setText(resources.getString(R.string.got) + " " + gotMoney + " + " + extraMoney);
                                gameOverScreen[13].setX(gameOverScreen[11].getX() + ((Text) gameOverScreen[11]).getWidth() + 3);
                            }
                        }));
                        ++wasSharedInThisGame;
                        sp.edit().putInt(SP_MONEY, sp.getInt(SP_MONEY, 0) + 30).apply();
                    }
                } catch (ActivityNotFoundException e) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, R.string.google_is_not_installed, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;
            case FB_SHARE:
                i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                // intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
                i.putExtra(Intent.EXTRA_TEXT, title + "" + link);
                // See if official Facebook app is found
                boolean facebookAppFound = false;
                List<ResolveInfo> matches = activity.getPackageManager().queryIntentActivities(i, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                        i.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }
                // As fallback, launch sharer.php in a browser
                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + link;
                    i = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                }

                activity.startActivityForResult(i, 0);

                ShadesApplication.getInstance().trackEvent(shareString, shareResultString, fbString);

                if (wasSharedInThisGame < 2 && isNetworkAvailable()) {
                    extraMoney += 30;
                    gameOverScreen[11].registerUpdateHandler(new TimerHandler(4, new ITimerCallback() {
                        @Override
                        public void onTimePassed(TimerHandler pTimerHandler) {
                            ((Text)gameOverScreen[11]).setText(resources.getString(R.string.got) + " " + gotMoney + " + " + extraMoney);
                            gameOverScreen[13].setX(gameOverScreen[11].getX() + ((Text) gameOverScreen[11]).getWidth() + 3);
                        }
                    }));
                    ++wasSharedInThisGame;
                    sp.edit().putInt(SP_MONEY, sp.getInt(SP_MONEY, 0) + 30).apply();
                }
                break;
            default:
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");

                List<ResolveInfo> resInfo = activity.getPackageManager().queryIntentActivities(share, 0);
                if (!resInfo.isEmpty()) {
                    List<Intent> targetedShareIntents = new ArrayList();
                    Intent targetedShareIntent;

                    for (ResolveInfo resolveInfo : resInfo) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        targetedShareIntent = new Intent(Intent.ACTION_SEND);
                        targetedShareIntent.setType("text/plain");

                        // Find twitter: com.twitter.android...
                        //title = getString(R.string.shareFirst) + " " + difStr + " " + getString(R.string.shareSecond)
                        // + " - " + gameBoard.score + getString(R.string.shareThird) + " %23android %23shades2.0 %23game";
                        if ("com.twitter.android".equals(packageName)) {
                            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msg);
                        } else if ("com.google.android.gm".equals(packageName)) {
                            targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, title);
                            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, Uri.encode(title + "\r\n" + link));
                        } else if ("com.android.email".equals(packageName)) {
                            targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, title);
                            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, Uri.encode(title + "\n" + link));
                        } else {
                            // Rest of Apps
                            targetedShareIntent.putExtra(Intent.EXTRA_TEXT, title);
                        }

                        targetedShareIntent.setPackage(packageName);
                        targetedShareIntents.add(targetedShareIntent);
                    }

                    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), resources.getString(R.string.shareDialog));
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                    activity.startActivityForResult(chooserIntent, 0);

                    ShadesApplication.getInstance().trackEvent(shareString, shareResultString, vkString + " or default share intent");
                }
                break;
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void addMoneyForFollow(final int id) {

        if (!sp.getBoolean(SP_SUBSCRIBED[id], false)) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt(SP_MONEY, sp.getInt(SP_MONEY, 0) + 120)
                    .putBoolean(SP_SUBSCRIBED[id], true).apply();

            moneyText.registerUpdateHandler(new TimerHandler(10f, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    moneyText.setText(sp.getInt(SP_MONEY, 0) + "");
                    moneyText.setX(getMoneyTextX());
                }
            }));
        }

    }

    //-------------------------------------------------//
    //-------------VISIBILITY AND ANIMATIONS-----------//
    //-------------------AND COORDINATE CHECK (text)---//

    private void moveToRight(Entity e) {
        e.registerEntityModifier(new MoveXModifier(anim_menu_out_time, e.getX(), SCREEN_WIDTH));
    }

    private void moveToLeft(Entity e, float toX) {
        e.registerEntityModifier(new MoveXModifier(anim_menu_out_time, e.getX(), toX));
    }

    private void setBtnPlayVis(final boolean vis) {
        if (isPlayBtnOut == vis) return;

        isPlayBtnOut = vis;

        if (!vis) {
            menuPlay.registerEntityModifier(new MoveXModifier(.2f,
                    menuPlay.getX(), menuPlay.getX() + SCREEN_WIDTH - btn_play_x));
            rectPlayBtn.registerEntityModifier(new MoveXModifier(.2f,
                    rectPlayBtn.getX(), SCREEN_WIDTH));
        } else {
            menuPlay.registerEntityModifier(new MoveXModifier(.2f,
                    menuPlay.getX(), btn_play_x));
            rectPlayBtn.registerEntityModifier(new MoveXModifier(.2f,
                    rectPlayBtn.getX(), menu_left_x));
        }
    }

    private void setMenuVis(final boolean vis) {
        if (vis)
            for (Sprite ic: icMenu) {
                gameHud.registerTouchArea(ic);
                moveToLeft(ic, menu_left_x);
            }
        else
            for (Sprite ic : icMenu) {
                gameHud.unregisterTouchArea(ic);
                moveToRight(ic);
            }
    }

    private void setShopVis(final boolean vis){
        if (isShopOut == vis) return;

        isShopOut = vis;

        if (vis) shopFragment.listViewShop.smoothScrollToPosition(0);


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!vis) frameShop.startAnimation(shopToLeft);
                else {
                    frameShop.setVisibility(View.VISIBLE);
                    frameShop.startAnimation(shopToRight);
                }
            }
        });
    }

    private void setSettingsVis(final boolean vis) {
        //12 overall
        if (isSettingsOut == vis) return;

        final float duration = .3f;
        isSettingsOut = vis;

        if (vis) {
            for (int i = 0; i < 7; ++i)
                ic_settings[i].registerEntityModifier(new MoveXModifier(duration, ic_settings[i].getX(), settings_margin_left));

            for (int i = 7; i < 9; ++i) {
                ic_settings[i].registerEntityModifier(new MoveXModifier(duration,
                        -((Rectangle)ic_settings[i]).getWidth(), settings_x_buttons));
            }

            ic_settings[9].registerEntityModifier(new MoveXModifier(duration,
                    -((Text) ic_settings[9]).getWidth(), settings_text_tutorial));
            ic_settings[10].registerEntityModifier(new MoveXModifier(duration,
                    -((Text) ic_settings[10]).getWidth(), settings_text_report));

            gameHud.registerTouchArea((Rectangle) ic_settings[1]);
            gameHud.registerTouchArea((Rectangle) ic_settings[7]);
            gameHud.registerTouchArea((Rectangle) ic_settings[8]);

        } else {
            for (int i = 0; i < 7; ++i) {
                if (i == 1 || i == 2) continue;
                ic_settings[i].registerEntityModifier(new MoveXModifier(duration, settings_margin_left,
                        -SCREEN_WIDTH / 2));
            }
            for (int i = 1; i < 3; ++i)
            ic_settings[i].registerEntityModifier(new MoveXModifier(duration, settings_margin_left,
                    - SCREEN_WIDTH));

            for (int i = 7; i < 9; ++i) {
                ic_settings[i].registerEntityModifier(new MoveXModifier(duration, settings_x_buttons,
                        -((Rectangle)ic_settings[i]).getWidth()));
            }

            ic_settings[9].registerEntityModifier(new MoveXModifier(duration, settings_text_tutorial
                    , -((Text) ic_settings[9]).getWidth()));
            ic_settings[10].registerEntityModifier(new MoveXModifier(duration, settings_text_report
                    , -((Text) ic_settings[10]).getWidth()));

            ((BaseGameActivity)activity).runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    gameHud.unregisterTouchArea((Rectangle) ic_settings[1]);
                    gameHud.unregisterTouchArea((Rectangle) ic_settings[7]);
                    gameHud.unregisterTouchArea((Rectangle) ic_settings[8]);
                }
            });
        }
    }

    private void setGroupsVis(boolean vis) {
        if (isGroupsOut == vis) return;

        //bottom counting from 0;0 of the top group icon
        final float bottom = origin_groups_y[0] + icGroups[0].getHeight();

        isGroupsOut = vis;
        if (vis)
            for (final mButtonSprite ic : icGroups) {
                ic.clearEntityModifiers();
                ic.setVisible(true);
                ic.registerEntityModifier(new ParallelEntityModifier(
                        new ScaleAtModifier(.4f, .2f, 1f, ic.getWidth(), bottom - ic.getY()),
                        new AlphaModifier(.4f, 0f, 1f) ));
                gameHud.registerTouchArea(ic);
            }
        else {
            for (final mButtonSprite ic : icGroups) {
                ic.clearEntityModifiers();
                ic.registerEntityModifier(new ParallelEntityModifier(
                        new ScaleAtModifier(.4f, 1f, .2f, ic.getWidth(), bottom - ic.getY()),
                        new AlphaModifier(.4f, 1f, 0f, new IEntityModifier.IEntityModifierListener() {
                            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {}
                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                ic.setVisible(false);
                            }
                        })
                ));
            }

            ((BaseGameActivity)activity).runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    gameHud.unregisterTouchArea(icGroups[0]);
                    gameHud.unregisterTouchArea(icGroups[1]);
                    gameHud.unregisterTouchArea(icGroups[2]);
                    gameHud.unregisterTouchArea(icGroups[3]);
                }
            });
        }
    }

    private void updateTextX(Text text) {
        text.setX((SCREEN_WIDTH - text.getWidth()) / 2f);
    }

    private float getMoneyTextX() {
        return left_menu_money_ic - moneyText.getWidth() - (float)px_per_inch/35f;
    }

    // ----------------------------------------------------//
    // ----------------------------------------------------//
    // ----------------------COLORS------------------------//
    // ----------------------------------------------------//
    // ----------------------------------------------------//

    private float[][] setColorArray(final int arrayNum) {
        if (arrayNum == currentColorArray && wasGamePlay) {
            return Colors;
        }

        final int colorSetPairs = 14;

        final int finalArrayNum = new Random().nextBoolean() ? arrayNum : arrayNum + colorSetPairs / 2;
        switch (finalArrayNum) {
            case 0:
                Colors = new float[][]{ColorArray.blue1, ColorArray.blue2, ColorArray.blue3, ColorArray.blue4, ColorArray.blue5, ColorArray.blueBg, ColorArray.blueBgLite};
                break;
            case 1:
                Colors = new float[][]{ColorArray.violet1, ColorArray.violet2, ColorArray.violet3, ColorArray.violet4, ColorArray.violet5, ColorArray.violetBg, ColorArray.violetBgLite};
                break;
            case 2:
                Colors = new float[][]{ColorArray.red1, ColorArray.red2, ColorArray.red3, ColorArray.red4, ColorArray.red5, ColorArray.redBg, ColorArray.redBgLite};
                break;
            case 3:
                Colors = new float[][]{ColorArray.orange1, ColorArray.orange2, ColorArray.orange3, ColorArray.orange4, ColorArray.orange5, ColorArray.orangeBg, ColorArray.orangeBgLite};
                break;
            case 4:
                Colors = new float[][]{ColorArray.turquoise1, ColorArray.turquoise2, ColorArray.turquoise3, ColorArray.turquoise4, ColorArray.turquoise5, ColorArray.turquoiseBg, ColorArray.turquoiseBgLite};
                break;
            case 5:
                Colors = new float[][]{ColorArray.green1, ColorArray.green2, ColorArray.green3, ColorArray.green4, ColorArray.green5, ColorArray.greenBg, ColorArray.greenBgLite};
                break;
            case 6:
//                Colors = new float[][]{ColorArray.rainbow1, ColorArray.rainbow2, ColorArray.rainbow3, ColorArray.rainbow4, ColorArray.rainbow5, ColorArray.rainbowBg, ColorArray.rainbowBgLite};
                Colors = new float[][]{ColorArray.yellow1, ColorArray.yellow2, ColorArray.yellow3, ColorArray.yellow4, ColorArray.yellow5, ColorArray.yellowBg, ColorArray.yellowBgLite};
                break;
            case 7:
                Colors = new float[][]{ColorArray.blue1, ColorArray.blue2, ColorArray.blue3, ColorArray.blue4, ColorArray.blue5, ColorArray.blueBgLite, ColorArray.blueBg};
                break;
            case 8:
                Colors = new float[][]{ColorArray.violet1, ColorArray.violet2, ColorArray.violet3, ColorArray.violet4, ColorArray.violet5, ColorArray.violetBgLite, ColorArray.violetBg};
                break;
            case 9:
                Colors = new float[][]{ColorArray.red1, ColorArray.red2, ColorArray.red3, ColorArray.red4, ColorArray.red5, ColorArray.redBgLite, ColorArray.redBg};
                break;
            case 10:
                Colors = new float[][]{ColorArray.orange1, ColorArray.orange2, ColorArray.orange3, ColorArray.orange4, ColorArray.orange5, ColorArray.orangeBgLite, ColorArray.orangeBg};
                break;
            case 11:
                Colors = new float[][]{ColorArray.turquoise1, ColorArray.turquoise2, ColorArray.turquoise3, ColorArray.turquoise4, ColorArray.turquoise5, ColorArray.turquoiseBgLite, ColorArray.turquoiseBg};
                break;
            case 12:
                Colors = new float[][]{ColorArray.green1, ColorArray.green2, ColorArray.green3, ColorArray.green4, ColorArray.green5, ColorArray.greenBgLite, ColorArray.greenBg};
                break;
            case 13:
//                Colors = new float[][]{ColorArray.rainbow1, ColorArray.rainbow2, ColorArray.rainbow3, ColorArray.rainbow4, ColorArray.rainbow5, ColorArray.rainbowBgLite, ColorArray.rainbowBg};
                Colors = new float[][]{ColorArray.yellow1, ColorArray.yellow2, ColorArray.yellow3, ColorArray.yellow4, ColorArray.yellow5, ColorArray.yellowBgLite, ColorArray.yellowBg};
                break;
        }
        currentColorArray = arrayNum;
        return Colors;
    }

    static class ColorArray {

        //COLOR BLUE
        /*
        #bbdefb
        #90caf9
        #42a5f5
        #1e88e5
        #1565c0
        bg
        #fff9c4
        #fffde7
        */
        final static float[] blue1 = {0.7333333333333333f, 0.8705882352941177f, 0.984313725490196f};
        final static float[] blue2 = {0.5647058823529412f, 0.792156862745098f, 0.9764705882352941f};
        final static float[] blue3 = {0.2588235294117647f, 0.6470588235294118f, 0.9607843137254902f};
        final static float[] blue4 = {0.11764705882352941f, 0.5333333333333333f, 0.8980392156862745f};
        final static float[] blue5 = {0.08235294117647059f, 0.396078431372549f, 0.7529411764705882f};
        final static float[] blueBg = {1.0f, 0.9764705882352941f, 0.7686274509803921f};
        final static float[] blueBgLite = {1.0f, 0.9921568627450981f, 0.9058823529411765f};

        //COLOR VIOLET
        /*
        #e1bee7
        #ce93d8
        #ba68c8
        #9c27b0
        #7b1fa2
        bg
        #ffe0b2
        #fff3e0
        */
        final static float[] violet1 = {0.8823529411764706f, 0.7450980392156863f, 0.9058823529411765f};
        final static float[] violet2 = {0.807843137254902f, 0.5764705882352941f, 0.8470588235294118f};
        final static float[] violet3 = {0.7294117647058823f, 0.40784313725490196f, 0.7843137254901961f};
        final static float[] violet4 = {0.611764705882353f, 0.15294117647058825f, 0.6901960784313725f};
        final static float[] violet5 = {0.4823529411764706f, 0.12156862745098039f, 0.6352941176470588f};
        final static float[] violetBg = {1.0f, 0.8784313725490196f, 0.6980392156862745f};
        final static float[] violetBgLite = {1.0f, 0.9529411764705882f, 0.8784313725490196f};

        //COLOR RED
        /*
        #ffcdd2
        #ef9a9a
        #e57373
        #f44336
        #d32f2f
        bg
        #f9fbe7
        #f0f4c3
        */
        final static float[] red1 = {1.0f, 0.803921568627451f, 0.8235294117647058f};
        final static float[] red2 = {0.9372549019607843f, 0.6039215686274509f, 0.6039215686274509f};
        final static float[] red3 = {0.8980392156862745f, 0.45098039215686275f, 0.45098039215686275f};
        final static float[] red4 = {0.9568627450980391f, 0.2627450980392157f, 0.21176470588235294f};
        final static float[] red5 = {0.8274509803921568f, 0.1843137254901961f, 0.1843137254901961f};
        final static float[] redBg = {0.9411764705882353f, 0.9568627450980391f, 0.7647058823529411f};
        final static float[] redBgLite = {0.9764705882352941f, 0.984313725490196f, 0.9058823529411765f};

        //COLOR ORANGE
        /*
        #FFCC80
        #FFB74D
        #FB8C00
        #EF6C00
        #E65100
        bg
        #FFF8E1
        #FFFDE7
        */
        final static float[] orange1 = {1.0f, 0.8f, 0.5019607843137255f};
        final static float[] orange2 = {1.0f, 0.7176470588235294f, 0.30196078431372547f};
        final static float[] orange3 = {0.984313725490196f, 0.5490196078431373f, 0.0f};
        final static float[] orange4 = {0.9372549019607843f, 0.4235294117647059f, 0.0f};
        final static float[] orange5 = {0.9019607843137255f, 0.3176470588235294f, 0.0f};
        final static float[] orangeBg = {1.0f, 0.9725490196078431f, 0.8823529411764706f};
        final static float[] orangeBgLite = {1.0f, 0.9921568627450981f, 0.9058823529411765f};

        //COLOR TURQUOISE
        /*
        #B2DFDB
        #4DB6AC
        #009688
        #00796B
        #004D40
        bg
        #E3F2FD
        #E1F5FE
         */
        final static float[] turquoise1 = {0.6980392156862745f, 0.8745098039215686f, 0.8588235294117647f};
        final static float[] turquoise2 = {0.30196078431372547f, 0.7137254901960784f, 0.6745098039215687f};
        final static float[] turquoise3 = {0.0f, 0.5882352941176471f, 0.5333333333333333f};
        final static float[] turquoise4 = {0.0f, 0.4745098039215686f, 0.4196078431372549f};
        final static float[] turquoise5 = {0.0f, 0.30196078431372547f, 0.25098039215686274f};
        final static float[] turquoiseBg = {0.8901960784313725f, 0.9490196078431372f, 0.9921568627450981f};
        final static float[] turquoiseBgLite = {0.8823529411764706f, 0.9607843137254902f, 0.996078431372549f};

        //COLOR GREEN
        /*
        #C8E6C9
        #81C784
        #4CAF50
        #388E3C
        #1B5E20
        bg
        #FFF8E1
        #FFFDE7
         */
        final static float[] green1 = {0.7843137254901961f, 0.9019607843137255f, 0.788235294117647f};
        final static float[] green2 = {0.5058823529411764f, 0.7803921568627451f, 0.5176470588235293f};
        final static float[] green3 = {0.2980392156862745f, 0.6862745098039216f, 0.3137254901960784f};
        final static float[] green4 = {0.2196078431372549f, 0.5568627450980392f, 0.23529411764705882f};
        final static float[] green5 = {0.10588235294117647f, 0.3686274509803922f, 0.12549019607843137f};
        final static float[] greenBg = {1.0f, 0.9725490196078431f, 0.8823529411764706f};
        final static float[] greenBgLite = {1.0f, 0.9921568627450981f, 0.9058823529411765f};

        final static float[] rainbow1 = {0.9568627450980391f, 0.2627450980392157f, 0.21176470588235294f};
        final static float[] rainbow2 = {1.0f, 0.8941176470588235f, 0.22352941176470587f};
        final static float[] rainbow3 = {0.3137254901960784f, 0.6901960784313725f, 0.30980392156862746f};
        final static float[] rainbow4 = {0.050980392156862744f, 0.6352941176470588f, 0.9529411764705882f};
        final static float[] rainbow5 = {0.396078431372549f, 0.2980392156862745f, 0.7215686274509804f};
        final static float[] rainbowBg = {0.5882352941176471f, 0.8823529411764706f, 0.9607843137254902f};
        final static float[] rainbowBgLite = {0.7764705882352941f, 0.7176470588235294f, 1.0f};

        final static float[] yellow1 = {1.0f, 0.9607843137254902f, 0.615686274509804f};
        final static float[] yellow2 = {1.0f, 0.9333333333333333f, 0.34509803921568627f};
        final static float[] yellow3 = {0.9921568627450981f, 0.8470588235294118f, 0.20784313725490194f};
        final static float[] yellow4 = {0.9764705882352941f, 0.6588235294117647f, 0.14509803921568626f};
        final static float[] yellow5 = {0.9607843137254902f, 0.4980392156862745f, 0.09019607843137255f};
        final static float[] yellowBg = {0.8627450980392157f, 0.9294117647058824f, 0.7843137254901961f};
        final static float[] yellowBgLite = {0.8823529411764706f, 0.9607843137254902f, 0.996078431372549f};
    }
}
