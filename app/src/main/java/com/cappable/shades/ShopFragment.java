package com.cappable.shades;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.andengine.entity.text.Text;

import java.util.LinkedList;

/**
 * Created by hotmeaty on 13.08.2015.
 */
public class ShopFragment extends Fragment {

    View rootView;
    ListView listViewShop;
    Text tvMoney;
    SharedPreferences sp;
    LinkedList<Integer> statesArray;

    int currentColor;
    final int numOfColorSets = 7;

    private static final String SP_MONEY = "money";
    private static final String SP_CURRENT_COLOR_ARRAY = "c_array";
    private static final String[] SP_STATES = {"c_states1", "c_states2",
            "c_states3", "c_states4", "c_states5", "c_states6", "c_states7", "c_states8", "c_states9"};//select, selected, buy

    private static final int SET_STATES_BUY = 1;//select, selected, buy
    private static final int SET_STATES_SELECT = 2;//select, selected, buy
    private static final int SET_STATES_SELECTED = 3;//select, selected, buy

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.shop_fragment, container);
        sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        currentColor = sp.getInt(SP_CURRENT_COLOR_ARRAY, 0);

        statesArray = new LinkedList<>();

//        This block helps to test shop. Sets money to 10000 and sets color bought like on the first launch
//        {
//        SharedPreferences.Editor editor1 = sp.edit();
//        for (int i = 0; i < images.length - 1; ++i)
//            editor1.remove(SP_STATES[i]);
//        editor1.putInt(SP_MONEY, 10000);
//        editor1.putInt(SP_CURRENT_COLOR_ARRAY, 0);
//        editor1.commit();
//        nColor = 0;
//        }

        //IF !contains num, adds. helps on updates of app
        SharedPreferences.Editor editor = sp.edit();
        for (int i = 0; i < images.length - 1; ++i) {
            if (!sp.contains(SP_STATES[i])) {
                if (i == 0) {
                    statesArray.add(SET_STATES_SELECTED);
                    editor.putInt(SP_STATES[i], SET_STATES_SELECTED);
                } else {
                    statesArray.add(SET_STATES_BUY);
                    editor.putInt(SP_STATES[i], SET_STATES_BUY);
                }
            } else {
                statesArray.add(sp.getInt(SP_STATES[i], 0));
            }
        }
        editor.apply();

        listViewShop = (ListView) rootView.findViewById(R.id.listViewShop);
        listViewShop.setAdapter(new ImageAdapter(getActivity().getApplicationContext()));

        return rootView;
    }

    // references to our images
    public Integer[] images = {
            R.drawable.shop_item_1, R.drawable.shop_item_2,
            R.drawable.shop_item_3, R.drawable.shop_item_4,
            R.drawable.shop_item_5, R.drawable.shop_item_6,
            R.drawable.shop_item_7,
//                R.drawable.shop_item_1, R.drawable.shop_item_1,
            R.drawable.shop_item_coming_soon
    };

    private Integer[] prices = {
            300, 400, 500,
            600, 600, 600,
            600
    };

    public void setTVMoney(Text TVMoney) {
        this.tvMoney = TVMoney;
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        Button[] btns = new Button[images.length];
        Resources res = getResources();

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return images.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View listView;
            if (convertView == null) {
                listView = getActivity().getLayoutInflater().inflate(R.layout.shop_item, parent, false);
            } else {
                listView = convertView;
            }

            ImageView iv = (ImageView) listView.findViewById(R.id.ivShop);//image of colors
            TextView tv = (TextView) listView.findViewById(R.id.tvShopPrice);//price of color group
            btns[position] = (Button) listView.findViewById(R.id.buttonState);//state (buy/select/selected)

            if (position < numOfColorSets) {//if layout != coming soon colors
                iv.setImageResource(images[position]);
                tv.setText(""+ prices[position]);

                switch (statesArray.get(position)) {
                    case SET_STATES_BUY:
                        btns[position].setText(res.getText(R.string.buy));
                        break;
                    case SET_STATES_SELECTED:
                        btns[position].setText(res.getText(R.string.selected));
                        break;
                    case SET_STATES_SELECT:
                        btns[position].setText(res.getText(R.string.select));
                }

                btns[position].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Init of pos of clicked btn
                        View parentRow = (View) v.getParent();
                        ListView listView = (ListView) parentRow.getParent();
                        final int position = listView.getPositionForView(parentRow);
                        //-------

                        //gets the state of item by btn
                        switch (statesArray.get(position)) {
                            case SET_STATES_BUY:
                                //if enough money
                                int moneyNum = sp.getInt(SP_MONEY, 0);
//                                int moneyNum = Integer.parseInt(String.valueOf(tvMoney.getText()));
                                if (moneyNum >= prices[position]) {
                                    moneyNum -= prices[position];
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putInt(SP_MONEY, moneyNum);
                                    editor.putInt(SP_STATES[position], SET_STATES_SELECT).apply();
                                    statesArray.set(position, SET_STATES_SELECT);
                                    btns[position].setText(res.getText(R.string.select));
                                    tvMoney.setText(moneyNum+"");
                                } else { // if not
                                    Toast.makeText(getActivity().getApplicationContext(), res.getText(R.string.not_enough_money),
                                            Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case SET_STATES_SELECT:
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putInt(SP_STATES[currentColor], SET_STATES_SELECT);
                                editor.putInt(SP_STATES[position], SET_STATES_SELECTED);
                                editor.putInt(SP_CURRENT_COLOR_ARRAY, position);
                                editor.apply();
                                statesArray.set(position, SET_STATES_SELECTED);
                                statesArray.set(currentColor, SET_STATES_SELECT);
                                btns[position].setText(res.getText(R.string.selected));
                                btns[currentColor].setText(res.getText(R.string.select));
                                currentColor = position;

                                //toast after restart show
                                Toast.makeText(getActivity().getApplicationContext(), res.getText(R.string.color_change),
                                        Toast.LENGTH_SHORT).show();
                                break;
                            case SET_STATES_SELECTED:
                                break;
                        }
                        notifyDataSetInvalidated();
                    }
                });
            } else {
                iv.setImageResource(images[position]);
                tv.setText("1kkk");
                btns[position].setText(res.getText(R.string.coming_soon));
                btns[position].setOnClickListener(null);
            }

            return listView;
        }
    }
}
