package com.cappable.shades;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;

/**
 * Created by root on 12/11/15.
 */
public class SplashScene extends BaseScene {

    private Text text, textInner;

    @Override
    public void createScene() {
        this.setBackground(new Background(0.21568627450980393f, 0.2784313725490196f, 0.30980392156862746f));

        text = new Text(0, 0, resourcesManager.font_splash_white, "Cappable", vbom);
        text.setPosition((SCREEN_WIDTH - text.getWidth()) / 2f, (SCREEN_HEIGHT - text.getHeight()) / 2f);
        attachChild(text);

        textInner = new Text(0,0, resourcesManager.font_splash_blue, "app", vbom);
        textInner.setPosition(text.getX() + text.getWidth()/7.9f, text.getY());
        attachChild(textInner);
    }

    public void onBackKeyPressed() {}

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_SPLASH;
    }

    @Override
    public void disposeScene() {
        text.detachSelf();
        textInner.detachSelf();
        text.dispose();
        textInner.dispose();
        this.detachSelf();
        this.dispose();
    }
}
