package com.cappable.shades;

import org.andengine.engine.Engine;
import org.andengine.ui.IGameInterface;

/**
 * Created by root on 12/11/15.
 */
public class SceneManager {

    private BaseScene splashScene, menuScene, tutorialScene;

    private static final SceneManager INSTANCE = new SceneManager();
    private SceneType currentSceneType = SceneType.SCENE_SPLASH;
    private BaseScene currentScene;
    private Engine engine = ResourcesManager.getInstance().engine;

    public enum SceneType
    {
        SCENE_SPLASH,
        SCENE_MENU,
        SCENE_TUTORIAL,
    }

    public void setScene(BaseScene scene) {
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }

    public void setScene(SceneType sceneType) {
        switch (sceneType) {
            case SCENE_MENU:
                setScene(menuScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            case SCENE_TUTORIAL:
                setScene(tutorialScene);
                break;
        }
    }

    public static SceneManager getInstance() {
        return INSTANCE;
    }

    public SceneType getCurrentSceneType() {
        return currentSceneType;
    }

    public BaseScene getCurrentScene() {
        return currentScene;
    }

    public void createSplashScene(IGameInterface.OnCreateSceneCallback pOnCreateSceneCallback) {
        ResourcesManager.getInstance().loadSplashScreen();
        splashScene = new SplashScene();
        currentScene = splashScene;
        pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
    }

    private void disposeSplashScene() {
        ResourcesManager.getInstance().unloadSplashScreen();
        splashScene.disposeScene();
        splashScene = null;
    }

    public void createMenuScene() {
        ResourcesManager.getInstance().loadMenuResources();
        menuScene = new MainMenuScene();
        setScene(menuScene);
        disposeSplashScene();
    }

    public void createTutorialScene(boolean fromMainScene) {
        ResourcesManager.getInstance().loadTutorialScene();
        tutorialScene = new TutorialScene(fromMainScene);
        setScene(tutorialScene);
    }

    public void disposeTutorialScene() {
        ResourcesManager.getInstance().unloadTutorialScene();
        setScene(menuScene);
        tutorialScene.disposeScene();
        tutorialScene.detachSelf();
        tutorialScene = null;
    }
}
